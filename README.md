NICO OSA Static
===

Licensing Information: READ LICENSE
---

Project source: https://AlexanderNico10@bitbucket.org/AlexanderNico10/osa-static.git
---

File List
---

```
README.md
404.php
android-sales-app.php
contact-us.php
contact-us-thanks.php
index.php
ipad-sales-app.php
mobile-case-studies.php
mobile-catalog.php
mobile-commerce-app.php
mobile-commerce-app-benefits.php
mobile-commerce-app-features.php
mobile-commerce-app-how-it-works.php
mobile-commerce-app-platforms.php
mobile-commerce-app-pricing.php
mobile-data-collection.php
mobile-data-collection-features.php
mobile-data-collection-systems.php
mobile-merchandising.php
mobile-merchandising-features.php
mobile-product-catalog-app.php
mobile-sales-app.php
mobile-sales-app-benefits.php
mobile-sales-app-features.php
mobile-sales-app-landing-1.php
mobile-sales-app-pricing.php
mobile-sales-apps-for-ipad-android-windows.php
mobile-sales-app-thanks.php
partner.php
privacy-policy.php
services.php
sign-up.php
sign-up-thanks.php
sitemap.php
user-guide-category.php
user-guide-detail.php
website-catalogue.php
who-we-are.php
./assets
```

```
/assets/css
ajax-loader.gif
bjqs.css
bootstrap.css
colorbox.css
demo.css
foundation.css
foundation.min.css
jquery.heroCarousel.css
medium.css
slick.css
small.css
style.css
./default
./fonts
```

```
/assets/css/default
arrows.png
bullets.png
default.css
loading.gif
```

```
/assets/css/fonts
slick.eot
slick.svg
slick.ttf
slick.woff
```

```
/assets/fonts
BebasNeue.eot
BebasNeue.svg
BebasNeue.ttf
BebasNeue.woff
opensans-bold.ttf
opensans-bold.woff
opensans-regular.eot
opensans-regular.ttf
opensans-regular.woff
```

```
/assets/js
app.js
bjqs-1.3.js
bootstrap.js
foundation.min.js
jquery.colorbox.js
jquery.easing-1.3.js
jquery.heroCarousel-1.3.js
jquery.heroCarousel-1.3.min.js
jquery.min.js
jquery.nivo.slider.js
jquery.pageslide.min.js
menu.js
slick.min.js
zepto.data.js
zepto.min.js
./foundation
./highlight
./libs
./vendor
```

How to view site
---

Ensure all files have been cloned
to local directory, then open index.php on your web browser.