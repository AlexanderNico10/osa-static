<!doctype html>
<head>
	<title>Pricing for Mobile Sales App - Onsight</title>
	<meta content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
	<meta name="description" content="Onsight is provided as a free mobile sales app for a single user. There is a low per-user monthly fee for more users.">
	<meta name="keywords" content="pricing; mobile business software; enterprise app; mobile sales" />
	<link href="http://getbootstrap.com/dist/css/bootstrap.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<link rel="stylesheet" media="screen and (max-width: 640px)" href="assets/css/small.css" />
	<link rel='stylesheet' media='screen and (min-width: 320px) and (max-width: 640px)' href='assets/css/small.css' />
	<link rel='stylesheet' media='screen and (min-width: 640px) and (max-width: 1024px)' href='assets/css/medium.css' />
	<link rel="stylesheet" href="assets/css/default/default.css" type="text/css" media="screen" />
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" ></script>
 	<script src="http://getbootstrap.com/dist/js/bootstrap.js"></script>
	
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
 	<script src="assets/js/jquery.min.js" ></script>	
 	<script src="assets/js/bootstrap.js" ></script>
</head>
<body>
<header>
	<div id="container">
		<div id="logo"><h1><a href="/">Onsightapp</a></h1></div>
		<div id="menu">
			<ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="mobile-sales-app.php" class="current">Mobile Sales</a>
					<ul>
						<li><a href="mobile-sales-app-features.php">App Features</a></li>
						<li><a href="mobile-sales-app-benefits.php">App Benefits</a></li>
						<li><a href="mobile-catalog.php">Mobile Catalogue</a></li>
						<li><a href="mobile-sales-apps-for-ipad-android-windows.php">Download Apps</a></li>
						<li><a href="mobile-sales-app-pricing.php">App Pricing</a></li>
					</ul>
				</li>
				<li><a href="mobile-merchandising.php">Mobile Merchandising</a>
					<ul>
						<li><a href="mobile-merchandising-features.php">App Features</a></li>
					</ul>
				</li>
				<li><a href="mobile-commerce-app.php">Mobile Commerce</a>
					<ul>
						<li><a href="mobile-commerce-app-features.php">Features</a></li>
						<li><a href="mobile-commerce-app-benefits.php">Benefits</a></li>
						<li><a href="mobile-commerce-app-how-it-works.php">How it works</a></li>
						<li><a href="mobile-commerce-app-platforms.php">Platforms</a></li>		
						<li><a href="mobile-commerce-app-pricing.php">Pricing</a></li>
					</ul>
				</li>
				<li><a href="mobile-case-studies.php">Case Studies</a></li>
				<li><a href="#">About</a>
					<ul>
						<li><a href="who-we-are.php">Who We Are</a></li>
						<li><a href="services.php">Our Services</a></li>
						<li><a href="partner.php">Partner Programme</a></li>
					</ul>
				</li>
				<li><a href="contact-us.php">Contact Us</a></li>
			</ul>
		</div>
		<nav class="navbar navbar-default" role="navigation">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">Onsightapp</a>
		    </div>
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li class="active"><a href="index.php">Home</a></li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mobile Sales <b class="caret"></b></a>
		          <ul class="dropdown-menu">
		            <li><a href="mobile-sales-app-features.php.php">App Features</a></li>
						<li><a href="mobile-sales-app-benefits.php">App Benefits</a></li>
						<li><a href="mobile-catalog.php">Mobile Catalogue</a></li>
						<li><a href="mobile-sales-apps-for-ipad-android-windows.php">Download Apps</a></li>
						<li><a href="mobile-sales-app-pricing.php">App Pricing</a></li>
		          </ul>
		        </li>
		          <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mobile Merchandising <b class="caret"></b></a>
		          <ul class="dropdown-menu">
		           	<li><a href="mobile-merchandising-features.php">App Features</a></li>
		          </ul>
		        </li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mobile Commerce <b class="caret"></b></a>
		          <ul class="dropdown-menu">
		           	<li><a href="mobile-commerce-app-features.php">Features</a></li>
					<li><a href="mobile-commerce-app-benefits.php">Benefits</a></li>
					<li><a href="mobile-commerce-app-how-it-works.php">How it works</a></li>
					<li><a href="mobile-commerce-app-platforms.php">Platforms</a></li>		
					<li><a href="mobile-commerce-app-pricing.php">Pricing</a></li>
		          </ul>
		        </li>
		        <li><a href="mobile-case-studies.php">Case Studies</a></li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">About<b class="caret"></b></a>
		          <ul class="dropdown-menu">
		        	<li><a href="who-we-are.php">Who We Are</a></li>
					<li><a href="services.php">Our Services</a></li>
					<li><a href="partner.php">Partner Programme</a></li>
		          </ul>
		        </li>
		        <li><a href="contact-us.php">Contact Us</a></li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	</div>
</header>
<div id="breadcrumbs">
	<ul>
		<li>Mobile Sales Application</li>
		<li>-</li>
		<li class="breadcrumb-blue">App Pricing</li>
	</ul>
</div>
<div id="wrapper">
	<div id="container" class="services no_padding">
		<div class="left width_100 pad_left">
			<h2>Mobile Sales Apps Pricing</h2>
			<p>
				Onsight's mobile sales application is provided as a cloud service with a low per-user monthly fee. We offer our fully-featured app for free to smaller firms. You can sign up and use Onsight for free for a year for a single user. No fees. No fuss. If you are a larger firm, sign up for our free tier to trial the app with a single user and then upgrade to our standard pricing tier whenever you are ready to add more users.
			</p>
		</div>
		<div class="right show_price">
			<form action="#" type="POST">
				<select name="select">
				  <option value="value1">USD</option> 
				  <option value="value2" selected>ZAR</option>
				</select>
			</form>
		</div>
	</div>
</div>
<div id="middle_grey">
	<div id="container" class="services">
		<div class="services_full pad-top">
			<table class="table1 pad_left">
		    <thead>
		        <tr>
		            <th></th>
		            <th scope="col" abbr="Starter" class="th_blue"><img class="width_100 width_tab width_25" src="assets/images/page_assets/mobile_sales/table_header1.png" alt=""></th>
		            <th scope="col" abbr="Medium" class="th_green"><img class="width_100 width_25 width_tab" src="assets/images/page_assets/mobile_sales/table_header2.png" alt=""></th>
		            <th scope="col" abbr="Business" class="th_orange"><img class="width_100 width_25 width_tab" src="assets/images/page_assets/mobile_sales/table_header3.png" alt=""></th>
		        </tr>
		    </thead>
		    <tbody>
		    	<tr>
		            <th scope="row">Product catalogue</th>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		        </tr>
		        <tr>
		            <th scope="row">Unlimited products</th>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		        </tr>
		        <tr>
		            <th scope="row">Product specs & manuals</th>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		        </tr>
		        <tr>
		            <th scope="row">Custom product fields</th>
		           	<td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		        </tr>
		        <tr>
		            <th scope="row">PDF catalogue generator</th>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		        </tr>
		        <tr>
		            <th scope="row">Create orders</th>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		        </tr>
		        <tr>
		            <th scope="row">Manage accounts</th>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		        </tr>
		         <tr>
		            <th scope="row">Manage contacts</th>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		        </tr>
		         <tr>
		            <th scope="row">Custom account fields</th>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		        </tr>
		         <tr>
		            <th scope="row">Discount rules</th>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		        </tr>
		         <tr>
		            <th scope="row">Mobile dashboard</th>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		        </tr>
		         <tr>
		            <th scope="row">Cloud solution</th>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		        </tr>
		         <tr>
		            <th scope="row">Full offline mode</th>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		        </tr>
		         <tr>
		            <th scope="row">iPad app</th>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		        </tr>
		         <tr>
		            <th scope="row">iPad Mini app</th>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		        </tr>
		         <tr>
		            <th scope="row">Android tablet app</th>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		        </tr>
		          <tr>
		            <th scope="row">Android phone app</th>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		        </tr>
		          <tr>
		            <th scope="row">Windows 8 app</th>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		        </tr>
		          <tr>
		            <th scope="row">Windows RT app</th>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		        </tr>
		    </tbody>
		</table>
		<!-- 2nd tab -->
		<table class="table2 pad_left">
		    <tfoot>
		        <tr>
		            <th scope="row"></th>
		            <td class="width_25"><a href="#"><img class="width_100 width_trial" src="assets/images/page_assets/mobile_sales/table_blue_btn.png" alt=""></a></td>
		            <td><a href="#"><img class="width_100 width_trial" src="assets/images/page_assets/mobile_sales/table_green_btn.png" alt=""></a></td>
		            <td><a href="#"><img class="width_100 width_trial" src="assets/images/page_assets/mobile_sales/table_orange_btn.png" alt=""></a></td>
		        </tr>
		    </tfoot>
		    <tbody class="table2width">
		        <tr>
		            <th scope="row" class="emptyRowWidth">Account Ownership</th>
		            <td>&nbsp;</td>
		            <td><span class="check"></span></td>
		            <td><span class="check"></span></td>
		        </tr>
		        <tr>
		            <th scope="row">Create Quotes</th>
		            <td>&nbsp;</td>
		            <td>&nbsp;</td>
		            <td><span class="check"></span></td>
		        </tr>
		        <tr>
		            <th scope="row">Custom quote and order templates</th>
		           	<td>&nbsp;</td>
		            <td>&nbsp;</td>
		            <td><span class="check"></span></td>
		        </tr>
		        <tr>
		            <th scope="row">Custom PDF catalogue templates</th>
		            <td>&nbsp;</td>
		            <td>&nbsp;</td>
		            <td><span class="check"></span></td>
		        </tr>
		        <tr>
		            <th scope="row">Import your existing printed brochures</th>
		            <td>&nbsp;</td>
		            <td>&nbsp;</td>
		            <td><span class="check"></span></td>
		        </tr>
		        <tr>
		            <th scope="row">Integration with your CRM and ERP systems</th>
		            <td>&nbsp;</td>
		            <td>&nbsp;</td>
		            <td><span class="check"></span></td>
		        </tr>
		         <tr>
		            <th scope="row">Access to a full REST API Customized to your exact requirements</th>
		            <td>&nbsp;</td>
		            <td>&nbsp;</td>
		            <td><span class="check"></span></td>
		        </tr>
		    </tbody>
		</table>
		</div>
	</div>
</div>
<footer>
	<div class="mid">
		<div id="footer_menu" class="display_none">
			<ul>
				<li><a href="#">Mobile Sales</a>
					<ul>
						<li><a href="mobile-sales-app.php">Overview</a></li>
						<li><a href="mobile-sales-app-features.php">App Features</a></li>
						<li><a href="mobile-sales-app-benefits.php">App Benefits</a></li>
						<li><a href="mobile-catalog.php">Mobile Catalogue</a></li>
						<li><a href="mobile-sales-apps-for-ipad-android-windows.php">Download Apps</a></li>
						<li><a href="mobile-sales-app-pricing.php">App Pricing</a></li>
					</ul>
				</li>
			</ul>
			<ul>
				<li><a href="#">Mobile Commerce</a>
					<ul>
						<li><a href="mobile-commerce-app.php">Overview</a></li>
						<li><a href="mobile-commerce-app-features.php">Features</a></li>
						<li><a href="mobile-commerce-app-benefits.php">Benefits</a></li>
						<li><a href="mobile-commerce-app-how-it-works.php">How it works</a></li>
						<li><a href="mobile-commerce-app-platforms.php">Platforms</a></li>
						<li><a href="mobile-commerce-app-pricing.php">Pricing</a></li>
					</ul>
				</li>
			</ul>
			<ul>
				<li><a href="#">Mobile Merchandising</a>
					<ul>
						<li><a href="mobile-merchandising.php">Overview</a></li>
						<li><a href="mobile-merchandising-features.php">Features</a></li>
					</ul>
				</li>
			</ul>
			<ul>
				<li><a href="#">About</a>
					<ul>
						<li><a href="who-we-are.php">Who we are</a></li>
						<li><a href="#">Our Blog</a></li>
						<li><a href="services.php">Our Services</a></li>
						<li><a href="partner.php">Partner Program</a></li>
					</ul>
				</li>
			</ul>
			<ul class="last_ul">
				<li><a href="mobile-case-studies.php">Case Studies</a></li>
				<li><a href="sign-up.php">Sign Up</a></li>
				<li><a href="#">My Account</a></li>
				<li><a href="contact-us.php">Contact</a></li>
				<li><a href="index.php">Home</a></li>
			</ul>
		</div>
		<div id="footer_left">
			<div id="footer_logo">
				<h1><a href="#">OnsightApp</a></h1>
			</div>
			<div id="social">
				<a href="#"><img src="assets/images/ux/facebook_ico.png"></a>
				<a href="#"><img src="assets/images/ux/google_ico.png"></a>
				<a href="#"><img src="assets/images/ux/twitter_ico.png"></a>
			</div>
			<div id="copy">
				<p>Copyright 2014</p>
			</div>
		</div>
	</div>
</footer>
<a class="sidetag quote_1" href="sign-up.php">
    SIGN UP NOW
</a>
<a class="sidetag signup_1" href="#getquote_sidetag">
    QUESTIONS ?
</a>
</body>
</html>