<!doctype html>
<head>
	<title>App Features of Mobile Sales App - Onsight</title>
	<meta content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
	<meta name="description" content="The Onsight mobile sales software provides a glossy mobile product catalogue, tools to manage client information, an order-entry and quote-generation system and a reporting dashboard.">
	<meta name="keywords" content="features; mobile sales app; product catalogue; crm; orders; quotes; contracts; proposals">
	<link href="http://getbootstrap.com/dist/css/bootstrap.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<link rel="stylesheet" media="screen and (max-width: 640px)" href="assets/css/small.css" />
	<link rel='stylesheet' media='screen and (min-width: 320px) and (max-width: 640px)' href='assets/css/small.css' />
	<link rel='stylesheet' media='screen and (min-width: 640px) and (max-width: 1024px)' href='assets/css/medium.css' />
	<link rel="stylesheet" href="assets/css/default/default.css" type="text/css" media="screen" />
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" ></script>
 	<script src="http://getbootstrap.com/dist/js/bootstrap.js"></script>
	<link rel="stylesheet" href="assets/css/colorbox.css" />
	<script src="assets/js/jquery.colorbox.js"></script>
	
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
 	<script src="assets/js/jquery.min.js" ></script>	
 	<script src="assets/js/bootstrap.js" ></script>
	
	<script>
		$(document).ready(function(){
			$(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390});
		});
	</script>
</head>
<body>
<header>
	<div id="container">
		<div id="logo"><h1><a href="/">Onsightapp</a></h1></div>
		<div id="menu">
			<ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="mobile-sales-app.php" class="current">Mobile Sales</a>
					<ul>
						<li><a href="mobile-sales-app-features.php">App Features</a></li>
						<li><a href="mobile-sales-app-benefits.php">App Benefits</a></li>
						<li><a href="mobile-catalog.php">Mobile Catalogue</a></li>
						<li><a href="mobile-sales-apps-for-ipad-android-windows.php">Download Apps</a></li>
						<li><a href="mobile-sales-app-pricing.php">App Pricing</a></li>
					</ul>
				</li>
				<li><a href="mobile-merchandising.php">Mobile Merchandising</a>
					<ul>
						<li><a href="mobile-merchandising-features.php">App Features</a></li>
					</ul>
				</li>
				<li><a href="mobile-commerce-app.php">Mobile Commerce</a>
					<ul>
						<li><a href="mobile-commerce-app-features.php">Features</a></li>
						<li><a href="mobile-commerce-app-benefits.php">Benefits</a></li>
						<li><a href="mobile-commerce-app-how-it-works.php">How it works</a></li>
						<li><a href="mobile-commerce-app-platforms.php">Platforms</a></li>		
						<li><a href="mobile-commerce-app-pricing.php">Pricing</a></li>
					</ul>
				</li>
				<li><a href="mobile-case-studies.php">Case Studies</a></li>
				<li><a href="#">About</a>
					<ul>
						<li><a href="who-we-are.php">Who We Are</a></li>
						<li><a href="services.php">Our Services</a></li>
						<li><a href="partner.php">Partner Programme</a></li>
					</ul>
				</li>
				<li><a href="contact-us.php">Contact Us</a></li>
			</ul>
		</div>
		<nav class="navbar navbar-default" role="navigation">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">Onsightapp</a>
		    </div>
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li class="active"><a href="index.php">Home</a></li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mobile Sales <b class="caret"></b></a>
		          <ul class="dropdown-menu">
		            <li><a href="mobile-sales-app-features.php.php">App Features</a></li>
						<li><a href="mobile-sales-app-benefits.php">App Benefits</a></li>
						<li><a href="mobile-catalog.php">Mobile Catalogue</a></li>
						<li><a href="mobile-sales-apps-for-ipad-android-windows.php">Download Apps</a></li>
						<li><a href="mobile-sales-app-pricing.php">App Pricing</a></li>
		          </ul>
		        </li>
		          <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mobile Merchandising <b class="caret"></b></a>
		          <ul class="dropdown-menu">
		           	<li><a href="mobile-merchandising-features.php">App Features</a></li>
		          </ul>
		        </li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mobile Commerce <b class="caret"></b></a>
		          <ul class="dropdown-menu">
		           	<li><a href="mobile-commerce-app-features.php">Features</a></li>
					<li><a href="mobile-commerce-app-benefits.php">Benefits</a></li>
					<li><a href="mobile-commerce-app-how-it-works.php">How it works</a></li>
					<li><a href="mobile-commerce-app-platforms.php">Platforms</a></li>		
					<li><a href="mobile-commerce-app-pricing.php">Pricing</a></li>
		          </ul>
		        </li>
		        <li><a href="mobile-case-studies.php">Case Studies</a></li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">About<b class="caret"></b></a>
		          <ul class="dropdown-menu">
		        	<li><a href="who-we-are.php">Who We Are</a></li>
					<li><a href="services.php">Our Services</a></li>
					<li><a href="partner.php">Partner Programme</a></li>
		          </ul>
		        </li>
		        <li><a href="contact-us.php">Contact Us</a></li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	</div>
</header>
<div id="breadcrumbs">
	<ul>
		<li>Mobile Sales Application</li>
		<li>-</li>
		<li class="breadcrumb-blue">App Features</li>
	</ul>
</div>
<div id="wrapper" class="auto_margin">
	<div id="container">
		<div class="left width_100 pad_left">
			<h2>Mobile App Features</h2>
			<p>The Onsight mobile sales app allows your sales reps to use a tablet to manage client information, demo your product catalogue, create quotes and place orders. Managers can also track the team's sales activities.</p>
			<div class="left noFloat cent">
				<a class='youtube' href="http://www.youtube.com/embed/MCe-QoE3Qa4?rel=0&wmode=transparent"><img class="fit fitfeat" src="assets/images/page_assets/mobile_sales/what_are_video.png"></a>
			</div>
		</div>
		<div class="right width_100">
			<div id="ytvideo1" style="display:none;">
				<iframe width="459" height="350" id="ytvideo" frameborder="0" allowfullscreen></iframe>
			</div>
			<img class="display_none fit" src="assets/images/page_assets/mobile_sales/puzzle_guy.jpg">
		</div>
	</div>
</div>

<div id="full_white">
	<div id="container" class="improve_sales_features">
		<div class="services_full center_mob padding_bottom_10 padding_top_20">
			<h2 class="center no_padding pad_left">Mobile Product Catalogue</h2>
			<h4 class="blue center">Showcase your products using a glossy, visually-appealing electronic catalogue.</h4>
			<ul>
				<li>
					<img class="logo_app_feat logo_dash" src="assets/images/page_assets/mobile_sales/brochure_pink.png">
					<h5>Showcase your Products</h5><p>using a glossy, visually-appealing electronic catalogue.</p>
				</li>
				<li>
					<img class="logo_app_feat logo_dash" src="assets/images/page_assets/mobile_sales/catalog_orange.png">
					<h5>Create a catalogue</h5><p>from a handful of products to 1,000's of products.</p>
				</li>
				<li>
					<img class="logo_app_feat logo_dash" src="assets/images/page_assets/mobile_sales/x_green.png">
					<h5>Import your Products</h5><p>from Excel using our fast product import tool.</p>
				</li>
				<li>
					<img class="logo_app_feat logo_dash" src="assets/images/page_assets/mobile_sales/finder_blue.png">
					<h5>Browse the Product Catalogue</h5><p>using touch gestures on your device.</p>
				</li>
				<li>
					<img class="logo_app_feat logo_dash" src="assets/images/page_assets/mobile_sales/refresh_green.png">
					<h5>All Catalogue Changes</h5><p>are immediately visible on all devices the next time they sync.</p>
				</li>
				<li>
					<img class="logo_app_feat logo_dash" src="assets/images/page_assets/mobile_sales/printer_green.png">
					<h5>Generate an up-to-date</h5><p>PDF product catalogue for emailing or printing.</p>
				</li>
			</ul>
			<a href="#"><button class="full_blue_btn center_full">Learn More</button></a>
		</div>
	</div>
</div>

<div id="light_grey">
	<div id="container">
		<div class="left pad-top width_100 pad_left">
			<h2>Product Brochures</h2>
			<ul class="blue_ticks">
			    <li>Convert your existing printed product brochures into electronic brochures.</li>
			    <li>Flip through the brochures using swipe gestures.</li>
			    <li>Tap on products in the brochure to see more details.</li>
			    <li>Add items to an order from within the brochure.</li>
			</ul>
		</div>
		<div class="right">
			<img class="display_none" src="assets/images/page_assets/mobile_sales/social_girl.jpg">
		</div>
	</div>
</div>

<div id="full_white">
	<div id="container">
		<div class="left width_100">
			<img class="display_none width_tab" src="assets/images/page_assets/mobile_sales/tablet_2.jpg">
		</div>
		<div class="right pad-top width_100 pad_left">
			<h2>Quotes & Orders</h2>
			<ul class="blue_ticks">
			    <li>Create a new quote or order whilst onsite at your client's premises.</li>
			    <li>Add items to the quote or order by browsing the product catalogue.</li>
			    <li>Send PDF quotes to your client for approval.</li>
			    <li>Send PDF orders to your office for processing.</li>
			    <li>Full order history is stored for all clients.</li>
			</ul>
		</div>
	</div>
</div>

<div id="light_grey">
	<div id="container">
		<div class="left margin_top_20 width_100 pad_left">
			<h2>Accounts & Contacts</h2>
			<ul class="blue_ticks">
			    <li>Manage all your customer information in one shared database.</li>
			    <li>Update the account and contact info from any mobile device.</li>
			    <li>Allocate accounts to specific sales agents.</li>
			    <li>All changes to contact information get synced to all other mobile devices</li>
			</ul>
		</div>
		<div class="right">
			<img class="display_none" src="assets/images/page_assets/mobile_sales/handshake.jpg">
		</div>
	</div>
</div>

<div id="full_white">
	<div id="container" class="improve_sales_features">
		<div class="services_full padding_bottom_10 padding_top_20 pad_left">
			<h2 class="center no_padding">Dashboard</h2>
			<ul class="dashboard_ul">
				<li>
					<img class="logo_app_feat" src="assets/images/page_assets/mobile_sales/track_sales.png">
					<h5>Showcase your Products</h5><p>using a glossy, visually-appealing electronic catalogue.</p>
				</li>
				<li>
					<img class="logo_app_feat" src="assets/images/page_assets/mobile_sales/green_eye.png">
					<h5>Create a catalogue</h5><p>from a handful of products to 1,000's of products.</p>
				</li>
			</ul>
		</div>
	</div>
</div>

<div id="middle_grey" class="padding_bottom_10">
	<div id="container">
		<div class="full application pad_left center_align">
			<h2 class="center pad-top no_padding">Try it Now for FREE</h2>
			<form action="#" id="free_trial">
				<input type="text" value="" placeholder="Full Name">
				<input type="text" value="" placeholder="Company Names">
				<input type="text" value="" placeholder="Email Address">
				<input type="text" value="" placeholder="Password">
				<a href="#"><input type="submit" value="Start Free Trial" class="green_btn"></a>
			</form>
			<p class="center">*We promise that your contact details will never be shared with anyone else! Read our <a href="#">Privacy Policy</a></p>
			<p class="center">*By signing up you agree to accept our <a href="#">Terms and Conditions</a></p>
			<ul>
				<li>
					<img class="logo_img" src="assets/images/page_assets/mobile_sales/money_back.png">
				</li>
				<li>
					<img class="logo_img" src="assets/images/page_assets/mobile_sales/free.png">
				</li>
				<li>
					<img class="logo_img" src="assets/images/page_assets/mobile_sales/no_card.png">
				</li>
			</ul>
		</div>
	</div>
</div>
<div id="full_white" class="margin_up">
	<div id="container">
		<div class="services_full need_more_info">
			<div class="left_40">
				<img class="display_none width_tab" src="assets/images/page_assets/mobile_sales/need_info.png">
			</div>
			<div class="right_60 width_100 pad_left">
				<h2>Complex requirements? Contact us to discuss</h2>
				<form action="#">
					<input type="text" name="full_name" value="" placeholder="Full Name">
					<input type="text" name="email" value="" placeholder="Email">
					<textarea placeholder="Please send me more information"></textarea>
					<span class="right_auto_width txt_right_106"><a href="#"><input type="submit" value="Submit" class="green_btn"></a></span>
				</form>
			</div>
		</div>
	</div>
</div>
<footer>
	<div class="mid">
		<div id="footer_menu" class="display_none">
			<ul>
				<li><a href="#">Mobile Sales</a>
					<ul>
						<li><a href="mobile-sales-app.php">Overview</a></li>
						<li><a href="mobile-sales-app-features.php">App Features</a></li>
						<li><a href="mobile-sales-app-benefits.php">App Benefits</a></li>
						<li><a href="mobile-catalog.php">Mobile Catalogue</a></li>
						<li><a href="mobile-sales-apps-for-ipad-android-windows.php">Download Apps</a></li>
						<li><a href="mobile-sales-app-pricing.php">App Pricing</a></li>
					</ul>
				</li>
			</ul>
			<ul>
				<li><a href="#">Mobile Commerce</a>
					<ul>
						<li><a href="mobile-commerce-app.php">Overview</a></li>
						<li><a href="mobile-commerce-app-features.php">Features</a></li>
						<li><a href="mobile-commerce-app-benefits.php">Benefits</a></li>
						<li><a href="mobile-commerce-app-how-it-works.php">How it works</a></li>
						<li><a href="mobile-commerce-app-platforms.php">Platforms</a></li>
						<li><a href="mobile-commerce-app-pricing.php">Pricing</a></li>
					</ul>
				</li>
			</ul>
			<ul>
				<li><a href="#">Mobile Merchandising</a>
					<ul>
						<li><a href="mobile-merchandising.php">Overview</a></li>
						<li><a href="mobile-merchandising-features.php">Features</a></li>
					</ul>
				</li>
			</ul>
			<ul>
				<li><a href="#">About</a>
					<ul>
						<li><a href="who-we-are.php">Who we are</a></li>
						<li><a href="#">Our Blog</a></li>
						<li><a href="services.php">Our Services</a></li>
						<li><a href="partner.php">Partner Program</a></li>
					</ul>
				</li>
			</ul>
			<ul class="last_ul">
				<li><a href="mobile-case-studies.php">Case Studies</a></li>
				<li><a href="sign-up.php">Sign Up</a></li>
				<li><a href="#">My Account</a></li>
				<li><a href="contact-us.php">Contact</a></li>
				<li><a href="index.php">Home</a></li>
			</ul>
		</div>
		<div id="footer_left">
			<div id="footer_logo">
				<h1><a href="#">OnsightApp</a></h1>
			</div>
			<div id="social">
				<a href="#"><img src="assets/images/ux/facebook_ico.png"></a>
				<a href="#"><img src="assets/images/ux/google_ico.png"></a>
				<a href="#"><img src="assets/images/ux/twitter_ico.png"></a>
			</div>
			<div id="copy">
				<p>Copyright 2014</p>
			</div>
		</div>
	</div>
</footer>
<a class="sidetag quote_1" href="sign-up.php">
    SIGN UP NOW
</a>
<a class="sidetag signup_1" href="#getquote_sidetag">
    QUESTIONS ?
</a>
<script type="text/javascript">
	// Popup window code
	function newPopup(url) {
		popupWindow = window.open(url,'popUpWindow','height=auto,width=auto,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
	}
</script>
<script type="text/javascript">
	$(document).on('click','#play_vid',function(e){
		$('#ytvideo1').show();
		$('.display_none').hide();
		$('#ytvideo').attr("src","http://www.youtube.com/embed/"+$(this).data('vid')+"?autoplay=1");
	});
</script>
</body>
</html>