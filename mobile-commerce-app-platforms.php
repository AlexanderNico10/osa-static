<!doctype html>
<head>
	<title>Mobile Commerce for iPad, Android and Windows Platforms - Onsight</title>
	<meta content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
	<meta name="description" content="Onsight provides mobile commerce apps that run on iPad, Android and Windows devices.">
	<meta name="keywords" content="mobile commerce app; product catalogue; orders; ordering; shopping cart; m-commerce; mcommerce" />
	<link href="http://getbootstrap.com/dist/css/bootstrap.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<link rel="stylesheet" media="screen and (max-width: 640px)" href="assets/css/small.css" />
	<link rel='stylesheet' media='screen and (min-width: 640px) and (max-width: 1024px)' href='assets/css/medium.css' />
	<link rel="stylesheet" href="assets/css/default/default.css" type="text/css" media="screen" />
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" ></script>
 	<script src="http://getbootstrap.com/dist/js/bootstrap.js"></script>
	
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
 	<script src="assets/js/jquery.min.js" ></script>	
 	<script src="assets/js/bootstrap.js" ></script>
</head>
<body>
<header>
	<div id="container">
		<div id="logo"><h1><a href="/">Onsightapp</a></h1></div>
		<div id="menu">
			<ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="mobile-sales-app.php">Mobile Sales</a>
					<ul>
						<li><a href="mobile-sales-app-features.php">App Features</a></li>
						<li><a href="mobile-sales-app-benefits.php">App Benefits</a></li>
						<li><a href="mobile-catalog.php">Mobile Catalogue</a></li>
						<li><a href="mobile-sales-apps-for-ipad-android-windows.php">Download Apps</a></li>
						<li><a href="mobile-sales-app-pricing.php">App Pricing</a></li>
					</ul>
				</li>
				<li><a href="mobile-merchandising.php">Mobile Merchandising</a>
					<ul>
						<li><a href="mobile-merchandising-features.php">App Features</a></li>
					</ul>
				</li>
				<li><a href="mobile-commerce-app.php" class="current">Mobile Commerce</a>
					<ul>
						<li><a href="mobile-commerce-app-features.php">Features</a></li>
						<li><a href="mobile-commerce-app-benefits.php">Benefits</a></li>
						<li><a href="mobile-commerce-app-how-it-works.php">How it works</a></li>
						<li><a href="mobile-commerce-app-platforms.php">Platforms</a></li>		
						<li><a href="mobile-commerce-app-pricing.php">Pricing</a></li>
					</ul>
				</li>
				<li><a href="mobile-case-studies.php">Case Studies</a></li>
				<li><a href="#">About</a>
					<ul>
						<li><a href="who-we-are.php">Who We Are</a></li>
						<li><a href="services.php">Our Services</a></li>
						<li><a href="partner.php">Partner Programme</a></li>
					</ul>
				</li>
				<li><a href="contact-us.php">Contact Us</a></li>
			</ul>
		</div>
		<nav class="navbar navbar-default" role="navigation">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">Onsightapp</a>
		    </div>
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li class="active"><a href="index.php">Home</a></li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mobile Sales <b class="caret"></b></a>
		          <ul class="dropdown-menu">
		            <li><a href="mobile-sales-app-features.php.php">App Features</a></li>
						<li><a href="mobile-sales-app-benefits.php">App Benefits</a></li>
						<li><a href="mobile-catalog.php">Mobile Catalogue</a></li>
						<li><a href="mobile-sales-apps-for-ipad-android-windows.php">Download Apps</a></li>
						<li><a href="mobile-sales-app-pricing.php">App Pricing</a></li>
		          </ul>
		        </li>
		          <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mobile Merchandising <b class="caret"></b></a>
		          <ul class="dropdown-menu">
		           	<li><a href="mobile-merchandising-features.php">App Features</a></li>
		          </ul>
		        </li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mobile Commerce <b class="caret"></b></a>
		          <ul class="dropdown-menu">
		           	<li><a href="mobile-commerce-app-features.php">Features</a></li>
					<li><a href="mobile-commerce-app-benefits.php">Benefits</a></li>
					<li><a href="mobile-commerce-app-how-it-works.php">How it works</a></li>
					<li><a href="mobile-commerce-app-platforms.php">Platforms</a></li>		
					<li><a href="mobile-commerce-app-pricing.php">Pricing</a></li>
		          </ul>
		        </li>
		        <li><a href="mobile-case-studies.php">Case Studies</a></li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">About<b class="caret"></b></a>
		          <ul class="dropdown-menu">
		        	<li><a href="who-we-are.php">Who We Are</a></li>
					<li><a href="services.php">Our Services</a></li>
					<li><a href="partner.php">Partner Programme</a></li>
		          </ul>
		        </li>
		        <li><a href="contact-us.php">Contact Us</a></li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	</div>
</header>
<div id="breadcrumbs">
	<ul>
		<li>Mobile Commerce</li>
		<li>-</li>
		<li class="breadcrumb-blue">Platforms</li>
	</ul>
</div>

<div id="full_white" class="border_bottom">
	<div id="container">
		<div class="left width_100 pad_left">
			<h2 class="padding_top_40">Mobile Commerce Apps for all Platforms</h2>
			<p>Onsight provides mobile commerce apps that run on all the major mobile platforms: iPad, Android and Windows. Showcase your products on the most sought-after mobile devices all over the world.</p>
		</div>
		<div class="right">
			<img class="display_none width_tab commerce_img_padding" src="assets/images/page_assets/mobile_com/three_platforms.png">
		</div>
	</div>
	<br style="clear:both;"/>
</div>

<div id="full_white" class="border_bottom">
	<div id="container">
		<div class="left pad-top width_100 pad_left">
			<h2>iPad and iPhone mobile commerce app</h2>
			<p class="width_90">Apple, the company behind both the iPad and the iPhone, has a loyal following around the world and is known for their "Think Different" slogan. All of Apple's mobile products make use of their uniquely designed iOS operating system. Apple's iPhone ushered in a new era of phones that go beyond merely making phone calls and messaging. Their iPad revolutionised the tablet market with its sleek design and lightweight structure. With billions of users worldwide, the Apple App Store is the ideal place to showcase your products and seek new customers. The Onsight iPad and iPhone mobile commerce app puts your products in front of this sought-after audience.</p>
		</div>
		<div class="right">
			<img class="display_none fit commerce_img_padding" src="assets/images/page_assets/mobile_com/apple.jpg">
		</div>
	</div>
</div>

<div id="full_white" class="border_bottom">
	<div id="container">
		<div class="left">
			<img class="display_none fit commerce_img_padding" src="assets/images/page_assets/mobile_com/android.jpg">
		</div>
		<div class="right pad-top width_100 pad_left">
			<h2>Android Mobile Commerce App</h2>
			<p class="width_90">Android is a Linux-based system that is designed mainly for touchscreen devices like tablets and smartphones. Many brands currently have devices with Android as their operating system. This includes, among others, the Samsung Galaxy series, HTC, Motorola and Sony Ericsson. Users with an Android-supported device can download apps from the Google Play online store, either for free or at a cost. There are currently over 500 000 apps available in the store. Add your own branded Android mobile commerce app to this list by using the Onsight platform.</p>
		</div>
	</div>
</div>

<div id="full_white" class="border_bottom">
	<div id="container">
		<div class="left pad-top width_100 pad_left">
			<h2>Windows phone and tablet mobile commerce apps</h2>
			<p class="width_90">Most, if not all of us, started our technological journey with the help of Microsoft and Windows. The release of the Windows Phone, Microsoft's take on the ideal smartphone, showed that they are still able to innovate. Use the Onsight platform to target Windows Phone users by publishing your mobile commerce app to the Windows Phone Marketplace. You can also target Windows tablet users by publishing your app to the Microsoft Store.</p>
		</div>
		<div class="right">
			<img class="display_none fit commerce_img_padding" src="assets/images/page_assets/mobile_com/windows.jpg">
		</div>
	</div>
</div>

<div class="margin_up" id="full_white">
	<div id="container">
		<div class="services_full need_more_info infoblock">
			<div class="left_40">
				<img class="display_none fit" src="assets/images/page_assets/mobile_sales/need_info.png">
			</div>
			<div class="right_60 width_100">
				<h2>Get more information on our mobile commerce app</h2>
				<form action="#">
					<input type="text" placeholder="Full Name" value="" name="full_name">
					<input type="text" placeholder="Email" value="" name="email">
					<textarea placeholder="Please send me more information on your mobile commerce app"></textarea>
					<p>Devices Required</p>
					<div>
						<div class="cblabel"><label><input type="checkbox" name="devices" value="" id="a"/>iPhone</label><br/>
							 <label><input type="checkbox" name="devices" value="" id="d"/>iPad</label>
						</div>
						<div class="cblabel"><label><input type="checkbox" name="devices" value="" id="b"/>Android Phone</label><br/>
							 <label><input type="checkbox" name="devices" value="" id="e"/>Android Tablet</label>
						</div>
						<div class="cblabel">
							 <label><input type="checkbox" name="devices" value="" id="f"/>Windows Tablet</label><br/>
							 <label><input type="checkbox" name="devices" value="" id="c"/>Windows Phone</label>
						</div>
					</div>
					<span class="right_auto_width txt_right_106"><a href="#"><input type="submit" value="Submit" class="green_btn"></a></span>
				</form>
			</div>
		</div>
	</div>
</div>
<footer>
	<div class="mid">
		<div id="footer_menu" class="display_none">
			<ul>
				<li><a href="#">Mobile Sales</a>
					<ul>
						<li><a href="mobile-sales-app.php">Overview</a></li>
						<li><a href="mobile-sales-app-features.php">App Features</a></li>
						<li><a href="mobile-sales-app-benefits.php">App Benefits</a></li>
						<li><a href="mobile-catalog.php">Mobile Catalogue</a></li>
						<li><a href="mobile-sales-apps-for-ipad-android-windows.php">Download Apps</a></li>
						<li><a href="mobile-sales-app-pricing.php">App Pricing</a></li>
					</ul>
				</li>
			</ul>
			<ul>
				<li><a href="#">Mobile Commerce</a>
					<ul>
						<li><a href="mobile-commerce-app.php">Overview</a></li>
						<li><a href="mobile-commerce-app-features.php">Features</a></li>
						<li><a href="mobile-commerce-app-benefits.php">Benefits</a></li>
						<li><a href="mobile-commerce-app-how-it-works.php">How it works</a></li>
						<li><a href="mobile-commerce-app-platforms.php">Platforms</a></li>
						<li><a href="mobile-commerce-app-pricing.php">Pricing</a></li>
					</ul>
				</li>
			</ul>
			<ul>
				<li><a href="#">Mobile Merchandising</a>
					<ul>
						<li><a href="mobile-merchandising.php">Overview</a></li>
						<li><a href="mobile-merchandising-features.php">Features</a></li>
					</ul>
				</li>
			</ul>
			<ul>
				<li><a href="#">About</a>
					<ul>
						<li><a href="who-we-are.php">Who we are</a></li>
						<li><a href="#">Our Blog</a></li>
						<li><a href="services.php">Our Services</a></li>
						<li><a href="partner.php">Partner Program</a></li>
					</ul>
				</li>
			</ul>
			<ul class="last_ul">
				<li><a href="mobile-case-studies.php">Case Studies</a></li>
				<li><a href="sign-up.php">Sign Up</a></li>
				<li><a href="#">My Account</a></li>
				<li><a href="contact-us.php">Contact</a></li>
				<li><a href="index.php">Home</a></li>
			</ul>
		</div>
		<div id="footer_left">
			<div id="footer_logo">
				<h1><a href="#">OnsightApp</a></h1>
			</div>
			<div id="social">
				<a href="#"><img src="assets/images/ux/facebook_ico.png"></a>
				<a href="#"><img src="assets/images/ux/google_ico.png"></a>
				<a href="#"><img src="assets/images/ux/twitter_ico.png"></a>
			</div>
			<div id="copy">
				<p>Copyright 2014</p>
			</div>
		</div>
	</div>
</footer>
<a class="sidetag quote_1" href="sign-up.php">
    SIGN UP NOW
</a>
<a class="sidetag signup_1" href="#getquote_sidetag">
    QUESTIONS ?
</a>
</body>
</html>