<!doctype html>
<head>
	<title>Mobile Merchandising Software Features - Onsight</title>
	<meta content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
	<meta name="description" content="The Onsight mobile merchandising system allows for custom form creation, allocation of store visits and the capturing of retail audits in-store.">
	<meta name="keywords" content="mobile merchandising app; retail audits" />
	<link href="http://getbootstrap.com/dist/css/bootstrap.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<link rel="stylesheet" media="screen and (max-width: 640px)" href="assets/css/small.css" />
	<link rel='stylesheet' media='screen and (min-width: 640px) and (max-width: 1024px)' href='assets/css/medium.css' />
	<link rel="stylesheet" href="assets/css/default/default.css" type="text/css" media="screen" />
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" ></script>
 	<script src="http://getbootstrap.com/dist/js/bootstrap.js"></script>
	
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
 	<script src="assets/js/jquery.min.js" ></script>	
 	<script src="assets/js/bootstrap.js" ></script>
</head>
<body>
<header>
	<div id="container">
		<div id="logo"><h1><a href="/">Onsightapp</a></h1></div>
		<div id="menu">
			<ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="mobile-sales-app.php">Mobile Sales</a>
					<ul>
						<li><a href="mobile-sales-app-features.php">App Features</a></li>
						<li><a href="mobile-sales-app-benefits.php">App Benefits</a></li>
						<li><a href="mobile-catalog.php">Mobile Catalogue</a></li>
						<li><a href="mobile-sales-apps-for-ipad-android-windows.php">Download Apps</a></li>
						<li><a href="mobile-sales-app-pricing.php">App Pricing</a></li>
					</ul>
				</li>
				<li><a href="mobile-merchandising.php" class="current">Mobile Merchandising</a>
					<ul>
						<li><a href="mobile-merchandising-features.php">App Features</a></li>
					</ul>
				</li>
				<li><a href="mobile-commerce-app.php">Mobile Commerce</a>
					<ul>
						<li><a href="mobile-commerce-app-features.php">Features</a></li>
						<li><a href="mobile-commerce-app-benefits.php">Benefits</a></li>
						<li><a href="mobile-commerce-app-how-it-works.php">How it works</a></li>
						<li><a href="mobile-commerce-app-platforms.php">Platforms</a></li>		
						<li><a href="mobile-commerce-app-pricing.php">Pricing</a></li>
					</ul>
				</li>
				<li><a href="mobile-case-studies.php">Case Studies</a></li>
				<li><a href="#">About</a>
					<ul>
						<li><a href="who-we-are.php">Who We Are</a></li>
						<li><a href="services.php">Our Services</a></li>
						<li><a href="partner.php">Partner Programme</a></li>
					</ul>
				</li>
				<li><a href="contact-us.php">Contact Us</a></li>
			</ul>
		</div>
		<nav class="navbar navbar-default" role="navigation">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">Onsightapp</a>
		    </div>
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li class="active"><a href="index.php">Home</a></li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mobile Sales <b class="caret"></b></a>
		          <ul class="dropdown-menu">
		            <li><a href="mobile-sales-app-features.php.php">App Features</a></li>
						<li><a href="mobile-sales-app-benefits.php">App Benefits</a></li>
						<li><a href="mobile-catalog.php">Mobile Catalogue</a></li>
						<li><a href="mobile-sales-apps-for-ipad-android-windows.php">Download Apps</a></li>
						<li><a href="mobile-sales-app-pricing.php">App Pricing</a></li>
		          </ul>
		        </li>
		          <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mobile Merchandising <b class="caret"></b></a>
		          <ul class="dropdown-menu">
		           	<li><a href="mobile-merchandising-features.php">App Features</a></li>
		          </ul>
		        </li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mobile Commerce <b class="caret"></b></a>
		          <ul class="dropdown-menu">
		           	<li><a href="mobile-commerce-app-features.php">Features</a></li>
					<li><a href="mobile-commerce-app-benefits.php">Benefits</a></li>
					<li><a href="mobile-commerce-app-how-it-works.php">How it works</a></li>
					<li><a href="mobile-commerce-app-platforms.php">Platforms</a></li>		
					<li><a href="mobile-commerce-app-pricing.php">Pricing</a></li>
		          </ul>
		        </li>
		        <li><a href="mobile-case-studies.php">Case Studies</a></li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">About<b class="caret"></b></a>
		          <ul class="dropdown-menu">
		        	<li><a href="who-we-are.php">Who We Are</a></li>
					<li><a href="services.php">Our Services</a></li>
					<li><a href="partner.php">Partner Programme</a></li>
		          </ul>
		        </li>
		        <li><a href="contact-us.php">Contact Us</a></li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	</div>
</header>
<div id="breadcrumbs">
	<ul>
		<li>Mobile Merchandising</li>
		<li>-</li>
		<li class="breadcrumb-blue"> Features</li>
	</ul>
</div>
<div id="wrapper">
	<div id="container">
		<div class="left width_100 width_50 pad_left pad_top">
			<h2>Merchandising App Features</h2>
			<p>The Onsight mobile merchandising system allows for custom form creation, allocation of store visits and the capturing of retail audits in-store.</p>
		</div>
		<div class="right">
			<img class="display_none width_tab" src="assets/images/page_assets/mobile_merch/icon_lady.jpg">
		</div>
	</div>
</div>

<div id="light_grey">
	<div id="container">
		<div class="left pad-top width_100 width_50 pad_left">
			<h2>Create Custom Forms</h2>
			<ul class="blue_ticks">
			    <li>Create your own unique audit forms in minutes</li>
			    <li>Or use one of our many pre-built forms</li>
			    <li>Choose from many different question types</li>
			</ul>
		</div>
		<div class="right">
			<img class="display_none width_tab" src="assets/images/page_assets/mobile_merch/coffee_tablet.jpg">
		</div>
	</div>
</div>

<div id="full_white">
	<div id="container" class="improve_sales_features">
		<div class="services_full pad_left padding_bottom_10 padding_top_20">
			<h2 class="center no_padding">Allocate & Track Store Visits</h2>
			<h4 class="blue center">Showcase your products using a glossy, visually-appealing electronic catalogue.</h4>
			<ul class="merch_width">
				<li>
					<img src="assets/images/page_assets/mobile_merch/walking_man.png">
					<h5>Create</h5><p>and allocate once-off store visits</p>
				</li>
				<li>
					<img src="assets/images/page_assets/mobile_merch/sync_man.png">
					<h5>Create</h5><p>and allocate recurring store visits</p>
				</li>
				<li>
					<img src="assets/images/page_assets/mobile_merch/green_globe.png">
					<h5>Track</h5><p>all store visits using GPS logging</p>
				</li>
				<li>
					<img src="assets/images/page_assets/mobile_merch/blue_clock.png">
					<h5>Store visits</h5><p>and audit activities are also time-stamped</p>
				</li>
			</ul>
		</div>
	</div>
</div>

<div id="light_grey">
	<div id="container">
		<div class="left">
			<img class="display_none width_tab" src="assets/images/page_assets/mobile_merch/man_tablet.png">
		</div>
		<div class="right pad_left margin_top_20 width_50 width_100">
			<h2>Mobile Data Capture</h2>
			<ul class="blue_ticks">
			    <li>Outside sales reps and merchandisers can view all required actions on an easy-to-read dashboard</li>
			    <li>All audit activities are wizard-driven</li>
			    <li>Data can be captured offline and will sync once the device is connected again.</li>
			</ul>
		</div>
	</div>
</div>

<div id="full_white" class="border_bottom">
	<div id="container">
		<div class="left pad-top width_100 pad_left width_50">
			<h2>Data Analysis & Reporting</h2>
			<ul class="blue_ticks">
			    <li>Data from all your reps and merchandisers is sent to a central database</li>
			    <li>Export data for reporting and further analysis</li>
			    <li>Graphs for numerical data</li>
			</ul>
		</div>
		<div class="right">
			<img class="display_none width_tab" src="assets/images/page_assets/mobile_merch/graph.jpg">
		</div>
	</div>
</div>

<div id="full_white" class="margin_up">
	<div id="container">
		<div class="services_full need_more_info">
			<div class="left_40">
				<img class="display_none width_tab" src="assets/images/page_assets/mobile_sales/need_info.png">
			</div>
			<div class="right_60 width_100">
				<h2>Get more information on our mobile merchandising app</h2>
				<form action="#">
					<input type="text" name="full_name" value="" placeholder="Full Name">
					<input type="text" name="email" value="" placeholder="Email">
					<textarea placeholder="Please send me more information on your mobile merchandising app"></textarea>
					<span class="right_auto_width txt_right_106"><a href="#"><input type="submit" class="green_btn" value="Submit"></a></span>
				</form>
			</div>
		</div>
	</div>
</div>
<footer>
	<div class="mid">
		<div id="footer_menu" class="display_none">
			<ul>
				<li><a href="#">Mobile Sales</a>
					<ul>
						<li><a href="mobile-sales-app.php">Overview</a></li>
						<li><a href="mobile-sales-app-features.php">App Features</a></li>
						<li><a href="mobile-sales-app-benefits.php">App Benefits</a></li>
						<li><a href="mobile-catalog.php">Mobile Catalogue</a></li>
						<li><a href="mobile-sales-apps-for-ipad-android-windows.php">Download Apps</a></li>
						<li><a href="mobile-sales-app-pricing.php">App Pricing</a></li>
					</ul>
				</li>
			</ul>
			<ul>
				<li><a href="#">Mobile Commerce</a>
					<ul>
						<li><a href="mobile-commerce-app.php">Overview</a></li>
						<li><a href="mobile-commerce-app-features.php">Features</a></li>
						<li><a href="mobile-commerce-app-benefits.php">Benefits</a></li>
						<li><a href="mobile-commerce-app-how-it-works.php">How it works</a></li>
						<li><a href="mobile-commerce-app-platforms.php">Platforms</a></li>
						<li><a href="mobile-commerce-app-pricing.php">Pricing</a></li>
					</ul>
				</li>
			</ul>
			<ul>
				<li><a href="#">Mobile Merchandising</a>
					<ul>
						<li><a href="mobile-merchandising.php">Overview</a></li>
						<li><a href="mobile-merchandising-features.php">Features</a></li>
					</ul>
				</li>
			</ul>
			<ul>
				<li><a href="#">About</a>
					<ul>
						<li><a href="who-we-are.php">Who we are</a></li>
						<li><a href="#">Our Blog</a></li>
						<li><a href="services.php">Our Services</a></li>
						<li><a href="partner.php">Partner Program</a></li>
					</ul>
				</li>
			</ul>
			<ul class="last_ul">
				<li><a href="mobile-case-studies.php">Case Studies</a></li>
				<li><a href="sign-up.php">Sign Up</a></li>
				<li><a href="#">My Account</a></li>
				<li><a href="contact-us.php">Contact</a></li>
				<li><a href="index.php">Home</a></li>
			</ul>
		</div>
		<div id="footer_left">
			<div id="footer_logo">
				<h1><a href="#">OnsightApp</a></h1>
			</div>
			<div id="social">
				<a href="#"><img src="assets/images/ux/facebook_ico.png"></a>
				<a href="#"><img src="assets/images/ux/google_ico.png"></a>
				<a href="#"><img src="assets/images/ux/twitter_ico.png"></a>
			</div>
			<div id="copy">
				<p>Copyright 2014</p>
			</div>
		</div>
	</div>
</footer>
<a class="sidetag quote_1" href="sign-up.php">
    SIGN UP NOW
</a>
<a class="sidetag signup_1" href="#getquote_sidetag">
    QUESTIONS ?
</a>
</body>
</html>