<!doctype html>
<head>
	<title>App Benefits of Mobile Sales App - Onsight</title>
	<meta content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">	
	<meta name="description" content="The Onsight mobile sales app helps save costs by eliminating paper processes; helps improve sales by letting you showcase your product catalogue to clients; and help you track and manage your sales staff.">
	<meta name="keywords" content="benefits; mobile sales app; product catalogue; crm; orders; quotes; contracts;catalogue proposals">
	<link href="http://getbootstrap.com/dist/css/bootstrap.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<link rel="stylesheet" media="screen and (max-width: 640px)" href="assets/css/small.css" />
	<link rel='stylesheet' media='screen and (min-width: 640px) and (max-width: 1024px)' href='assets/css/medium.css' />
	<link rel="stylesheet" href="assets/css/default/default.css" type="text/css" media="screen" />
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" ></script>
 	<script src="http://getbootstrap.com/dist/js/bootstrap.js"></script>
	
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
 	<script src="assets/js/jquery.min.js" ></script>	
 	<script src="assets/js/bootstrap.js" ></script>
</head>
<body>
<header>
	<div id="container">
		<div id="logo"><h1><a href="/">Onsightapp</a></h1></div>
		<div id="menu">
			<ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="mobile-sales-app.php" class="current">Mobile Sales</a>
					<ul>
						<li><a href="mobile-sales-app-features.php">App Features</a></li>
						<li><a href="mobile-sales-app-benefits.php">App Benefits</a></li>
						<li><a href="mobile-catalog.php">Mobile Catalogue</a></li>
						<li><a href="mobile-sales-apps-for-ipad-android-windows.php">Download Apps</a></li>
						<li><a href="mobile-sales-app-pricing.php">App Pricing</a></li>
					</ul>
				</li>
				<li><a href="mobile-merchandising.php">Mobile Merchandising</a>
					<ul>
						<li><a href="mobile-merchandising-features.php">App Features</a></li>
					</ul>
				</li>
				<li><a href="mobile-commerce-app.php">Mobile Commerce</a>
					<ul>
						<li><a href="mobile-commerce-app-features.php">Features</a></li>
						<li><a href="mobile-commerce-app-benefits.php">Benefits</a></li>
						<li><a href="mobile-commerce-app-how-it-works.php">How it works</a></li>
						<li><a href="mobile-commerce-app-platforms.php">Platforms</a></li>		
						<li><a href="mobile-commerce-app-pricing.php">Pricing</a></li>
					</ul>
				</li>
				<li><a href="mobile-case-studies.php">Case Studies</a></li>
				<li><a href="#">About</a>
					<ul>
						<li><a href="who-we-are.php">Who We Are</a></li>
						<li><a href="services.php">Our Services</a></li>
						<li><a href="partner.php">Partner Programme</a></li>
					</ul>
				</li>
				<li><a href="contact-us.php">Contact Us</a></li>
			</ul>
		</div>
		<nav class="navbar navbar-default" role="navigation">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">Onsightapp</a>
		    </div>
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li class="active"><a href="index.php">Home</a></li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mobile Sales <b class="caret"></b></a>
		          <ul class="dropdown-menu">
		            <li><a href="mobile-sales-app-features.php.php">App Features</a></li>
						<li><a href="mobile-sales-app-benefits.php">App Benefits</a></li>
						<li><a href="mobile-catalog.php">Mobile Catalogue</a></li>
						<li><a href="mobile-sales-apps-for-ipad-android-windows.php">Download Apps</a></li>
						<li><a href="mobile-sales-app-pricing.php">App Pricing</a></li>
		          </ul>
		        </li>
		          <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mobile Merchandising <b class="caret"></b></a>
		          <ul class="dropdown-menu">
		           	<li><a href="mobile-merchandising-features.php">App Features</a></li>
		          </ul>
		        </li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mobile Commerce <b class="caret"></b></a>
		          <ul class="dropdown-menu">
		           	<li><a href="mobile-commerce-app-features.php">Features</a></li>
					<li><a href="mobile-commerce-app-benefits.php">Benefits</a></li>
					<li><a href="mobile-commerce-app-how-it-works.php">How it works</a></li>
					<li><a href="mobile-commerce-app-platforms.php">Platforms</a></li>		
					<li><a href="mobile-commerce-app-pricing.php">Pricing</a></li>
		          </ul>
		        </li>
		        <li><a href="mobile-case-studies.php">Case Studies</a></li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">About<b class="caret"></b></a>
		          <ul class="dropdown-menu">
		        	<li><a href="who-we-are.php">Who We Are</a></li>
					<li><a href="services.php">Our Services</a></li>
					<li><a href="partner.php">Partner Programme</a></li>
		          </ul>
		        </li>
		        <li><a href="contact-us.php">Contact Us</a></li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	</div>
</header>
<div id="breadcrumbs">
	<ul>
		<li>Mobile Sales Application</li>
		<li>-</li>
		<li class="breadcrumb-blue">App Benefits</li>
	</ul>
</div>
<div id="wrapper">
	<div id="container">
		<div class="left width_100 pad_left pad-align">
			<h2>Save costs, improve sales, track activities</h2>
			<p class="width_75 width_100">The Onsight mobile sales app helps save costs by eliminating paper processes; helps improve sales by letting you showcase your product catalogue to clients; and helps you track and manage your sales staff.</p>
		</div>
		<div class="right">
			<img class="display_none width_tab" src="assets/images/page_assets/mobile_sales/lady_ready.jpg">
		</div>
	</div>
</div>

<div id="full_white">
	<div id="container" class="improve_sales_features">
		<div class="services_full padding_bottom_10 padding_top_20 pad_left">
			<h2 class="center no_padding">Impress your clients with an awesome-looking product catalogue</h2>
			<ul class="ben_width width_75 width_tab width100">
				<li>
					<img class="centerIMG" src="assets/images/page_assets/mobile_sales/ben_book.png">
					<h5>Get a visually-appealing</h5><p>electronic product catalogue with high-impact, glossy images</p>
				</li>
				<li>
					<img class="centerIMG" src="assets/images/page_assets/mobile_sales/ben_tab.png">
					<h5>Present</h5><p>product information to clients easily</p>
				</li>
				<li>
					<img class="centerIMG" src="assets/images/page_assets/mobile_sales/ben_green.png">
					<h5>Zoom in</h5><p>on product images for high definition detail</p>
				</li>
				<li>
					<img class="centerIMG" src="assets/images/page_assets/mobile_sales/ben_paper_plane.png">
					<h5>Generate & send</h5><p>up-to-date pdf product catalogues easily</p>
				</li>
			</ul>
		</div>
	</div>
</div>

<div id="light_grey">
	<div id="container">
		<div class="left">
			<img class="display_none width_tab" src="assets/images/page_assets/mobile_sales/ben_doc.jpg">
		</div>
		<div class="right pad-top width_100 pad_left">
			<h2>Save time and money by eliminating printed catalogues</h2>
			<ul class="blue_ticks">
			    <li>No more printing costs. No more embarrassing printing errors.</li>
			    <li>Eliminate paper usage and save the environment.</li>
			    <li>Easily update and maintain your catalogue yourself.</li>
			    <li>Sync product updates with your team instantly.</li>
			</ul>
		</div>
	</div>
</div>

<div id="full_white">
	<div id="container" class="improve_sales_features">
		<div class="services_full padding_bottom_10 padding_top_20 pad_left">
			<h2 class="center no_padding">Manage and share client information with your entire team</h2>
			<ul class="dashboard_ul">
				<li>
					<img class="logo_misc" src="assets/images/page_assets/mobile_sales/ben_share.png">
					<h5>Share accurate</h5><p>client contact information on any connected device.</p>
				</li>
				<li>
					<img class="logo_misc" src="assets/images/page_assets/mobile_sales/ben_sync.png">
					<h5>Sync client</h5><p>updates with your team instantly.</p>
				</li>
			</ul>
		</div>
	</div>
</div>

<div id="light_grey">
	<div id="container">
		<div class="left margin_top_20 width_100 pad_left">
			<h2>Close deals faster with instant quotes and orders</h2>
			<ul class="blue_ticks">
			    <li>Allow your sales staff to close deals on-site with clients.</li>
			    <li>Send quotes and orders to your clients quickly and easily.</li>
			    <li>No more delays in processing new orders.</li>
			</ul>
		</div>
		<div class="right">
			<img class="display_none width_tab" src="assets/images/page_assets/mobile_sales/ben_shake.jpg">
		</div>
	</div>
</div>

<div id="full_white">
	<div id="container" class="improve_sales_features">
		<div class="services_full padding_bottom_10 padding_top_20">
			<h2 class="center no_padding pad_left">Save costs by eliminating printed quotes and orders</h2>
			<ul class="dashboard_ul">
				<li>
					<img class="logo_misc centerIMG" src="assets/images/page_assets/mobile_sales/ben_printer.png">
					<h5>Eliminate printing costs</h5><p>Generate electronic quotes and orders directly on your tablet</p>
				</li>
				<li>
					<img class="logo_misc centerIMG" src="assets/images/page_assets/mobile_sales/ben_green_cloud.png">
					<h5>Get instant access</h5><p>to all your quotes and orders via the cloud.</p>
				</li>
			</ul>
		</div>
	</div>
</div>

<div id="light_grey">
	<div id="container">
		<div class="left pad-top width_100 pad_left">
			<h2>Real-Time Sales Monitoring</h2>
			<ul class="blue_ticks">
			    <li>Get an at-a-glance view of your all your sales activities.</li>
			    <li>Monitor your sales team with an easy-to-read dashboard.</li>
			    <li>Identify and tackle problems as soon as they appear.</li>
			</ul>
		</div>
		<div class="right">
			<img class="display_none width_tab" src="assets/images/page_assets/mobile_sales/social_girl.jpg">
		</div>
	</div>
</div>

<div id="full_white" class="margin_up">
	<div id="container">
		<div class="services_full need_more_info">
			<div class="left_40">
				<img class="display_none width_tab" src="assets/images/page_assets/mobile_sales/need_info.png">
			</div>
			<div class="right_60 width_100">
				<h2>Complex requirements? Contact us to discuss</h2>
				<form action="#">
					<input type="text" name="full_name" value="" placeholder="Full Name">
					<input type="text" name="email" value="" placeholder="Email">
					<textarea placeholder="Please send me more information"></textarea>
					<span class="right_auto_width txt_right_106"><a href="#"><input type="submit" value="Submit" class="green_btn"></a></span>
				</form>
			</div>
		</div>
	</div>
</div>
<footer>
	<div class="mid">
		<div id="footer_menu" class="display_none">
			<ul>
				<li><a href="#">Mobile Sales</a>
					<ul>
						<li><a href="mobile-sales-app.php">Overview</a></li>
						<li><a href="mobile-sales-app-features.php">App Features</a></li>
						<li><a href="mobile-sales-app-benefits.php">App Benefits</a></li>
						<li><a href="mobile-catalog.php">Mobile Catalogue</a></li>
						<li><a href="mobile-sales-apps-for-ipad-android-windows.php">Download Apps</a></li>
						<li><a href="mobile-sales-app-pricing.php">App Pricing</a></li>
					</ul>
				</li>
			</ul>
			<ul>
				<li><a href="#">Mobile Commerce</a>
					<ul>
						<li><a href="mobile-commerce-app.php">Overview</a></li>
						<li><a href="mobile-commerce-app-features.php">Features</a></li>
						<li><a href="mobile-commerce-app-benefits.php">Benefits</a></li>
						<li><a href="mobile-commerce-app-how-it-works.php">How it works</a></li>
						<li><a href="mobile-commerce-app-platforms.php">Platforms</a></li>
						<li><a href="mobile-commerce-app-pricing.php">Pricing</a></li>
					</ul>
				</li>
			</ul>
			<ul>
				<li><a href="#">Mobile Merchandising</a>
					<ul>
						<li><a href="mobile-merchandising.php">Overview</a></li>
						<li><a href="mobile-merchandising-features.php">Features</a></li>
					</ul>
				</li>
			</ul>
			<ul>
				<li><a href="#">About</a>
					<ul>
						<li><a href="who-we-are.php">Who we are</a></li>
						<li><a href="#">Our Blog</a></li>
						<li><a href="services.php">Our Services</a></li>
						<li><a href="partner.php">Partner Program</a></li>
					</ul>
				</li>
			</ul>
			<ul class="last_ul">
				<li><a href="mobile-case-studies.php">Case Studies</a></li>
				<li><a href="sign-up.php">Sign Up</a></li>
				<li><a href="#">My Account</a></li>
				<li><a href="contact-us.php">Contact</a></li>
				<li><a href="index.php">Home</a></li>
			</ul>
		</div>
		<div id="footer_left">
			<div id="footer_logo">
				<h1><a href="#">OnsightApp</a></h1>
			</div>
			<div id="social">
				<a href="#"><img src="assets/images/ux/facebook_ico.png"></a>
				<a href="#"><img src="assets/images/ux/google_ico.png"></a>
				<a href="#"><img src="assets/images/ux/twitter_ico.png"></a>
			</div>
			<div id="copy">
				<p>Copyright 2014</p>
			</div>
		</div>
	</div>
</footer>
<a class="sidetag quote_1" href="sign-up.php">
    SIGN UP NOW
</a>
<a class="sidetag signup_1" href="#getquote_sidetag">
    QUESTIONS ?
</a>
</body>
</html>