<!doctype html>
<head>
	<title>Case Studies - Onsight</title>
	<meta content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
	<meta name="description" content="The Onsight Mobile Commerce platform makes it easy to sell your products via mobile phones.">
	<meta name="keywords" content="mobile commerce app; product catalogue; orders; ordering; shopping cart; m-commerce; mcommerce"/>
	<link href="http://getbootstrap.com/dist/css/bootstrap.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="assets/css/slick.css"/>
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<link rel="stylesheet" media="screen and (max-width: 640px)" href="assets/css/small.css" />
	<link rel='stylesheet' media='screen and (min-width: 640px) and (max-width: 1024px)' href='assets/css/medium.css' />
	<link rel="stylesheet" href="assets/css/default/default.css" type="text/css" media="screen" />
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" ></script>
 	<script src="http://getbootstrap.com/dist/js/bootstrap.js"></script>
	<link rel="stylesheet" href="assets/css/colorbox.css" />
	<script src="assets/js/jquery.colorbox.js"></script>
	
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
 	<script src="assets/js/jquery.min.js" ></script>	
 	<script src="assets/js/bootstrap.js" ></script>
	
	<script>
		$(document).ready(function(){
			$(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390});
		});
	</script>
</head>
<body>
<header>
	<div id="container">
		<div id="logo"><h1><a href="/">Onsightapp</a></h1></div>
		<div id="menu">
			<ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="mobile-sales-app.php">Mobile Sales</a>
					<ul>
						<li><a href="mobile-sales-app-features.php">App Features</a></li>
						<li><a href="mobile-sales-app-benefits.php">App Benefits</a></li>
						<li><a href="mobile-catalog.php">Mobile Catalogue</a></li>
						<li><a href="mobile-sales-apps-for-ipad-android-windows.php">Download Apps</a></li>
						<li><a href="mobile-sales-app-pricing.php">App Pricing</a></li>
					</ul>
				</li>
				<li><a href="mobile-merchandising.php">Mobile Merchandising</a>
					<ul>
						<li><a href="mobile-merchandising-features.php">App Features</a></li>
					</ul>
				</li>
				<li><a href="mobile-commerce-app.php">Mobile Commerce</a>
					<ul>
						<li><a href="mobile-commerce-app-features.php">Features</a></li>
						<li><a href="mobile-commerce-app-benefits.php">Benefits</a></li>
						<li><a href="mobile-commerce-app-how-it-works.php">How it works</a></li>
						<li><a href="mobile-commerce-app-platforms.php">Platforms</a></li>		
						<li><a href="mobile-commerce-app-pricing.php">Pricing</a></li>
					</ul>
				</li>
				<li><a href="mobile-case-studies.php" class="current">Case Studies</a></li>
				<li><a href="#">About</a>
					<ul>
						<li><a href="who-we-are.php">Who We Are</a></li>
						<li><a href="services.php">Our Services</a></li>
						<li><a href="partner.php">Partner Programme</a></li>
					</ul>
				</li>
				<li><a href="contact-us.php">Contact Us</a></li>
			</ul>
		</div>
		<nav class="navbar navbar-default" role="navigation">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">Onsightapp</a>
		    </div>
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li class="active"><a href="index.php">Home</a></li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mobile Sales <b class="caret"></b></a>
		          <ul class="dropdown-menu">
		            <li><a href="mobile-sales-app-features.php.php">App Features</a></li>
						<li><a href="mobile-sales-app-benefits.php">App Benefits</a></li>
						<li><a href="mobile-catalog.php">Mobile Catalogue</a></li>
						<li><a href="mobile-sales-apps-for-ipad-android-windows.php">Download Apps</a></li>
						<li><a href="mobile-sales-app-pricing.php">App Pricing</a></li>
		          </ul>
		        </li>
		          <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mobile Merchandising <b class="caret"></b></a>
		          <ul class="dropdown-menu">
		           	<li><a href="mobile-merchandising-features.php">App Features</a></li>
		          </ul>
		        </li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mobile Commerce <b class="caret"></b></a>
		          <ul class="dropdown-menu">
		           	<li><a href="mobile-commerce-app-features.php">Features</a></li>
					<li><a href="mobile-commerce-app-benefits.php">Benefits</a></li>
					<li><a href="mobile-commerce-app-how-it-works.php">How it works</a></li>
					<li><a href="mobile-commerce-app-platforms.php">Platforms</a></li>		
					<li><a href="mobile-commerce-app-pricing.php">Pricing</a></li>
		          </ul>
		        </li>
		        <li><a href="mobile-case-studies.php">Case Studies</a></li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">About<b class="caret"></b></a>
		          <ul class="dropdown-menu">
		        	<li><a href="who-we-are.php">Who We Are</a></li>
					<li><a href="services.php">Our Services</a></li>
					<li><a href="partner.php">Partner Programme</a></li>
		          </ul>
		        </li>
		        <li><a href="contact-us.php">Contact Us</a></li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	</div>
</header>
<div id="breadcrumbs">
	<ul>
		<li>Case Studies</li>
	</ul>
</div>
<!-- Add slider here -->
<div id="wrapper">
	<div id="sub_content" class="testimonials display_none">
		<div id="case_scroller" class="quotes">
			<div class="slider single-item">
				<div class="article"><p>"I found the app a very useful tool in showcasing my product range."</p><p>Siggi Helgason,<br/>Owner<br/>Aurora Experience Inc</p></div>
				<div class="article"><p>"I would recommend any field-based sales team to try the Onsight app."</p><p>Ryan Cox,<br/>Sales Manager<br/>Lewis of London</p></div>
				<div class="article"><p>"Onsight allows me to update my product line at any time."</p><p>Sacha Alagich,<br/>Founder and Owner<br/>Escape to Paradise</p></div>
				<div class="article"><p>"The Onsight mobile sales system is helping us cope with our rapid growth."</p><p>Steve Steinhardt,<br/>Managing Director<br/>Steins Foods Ltd</p></div>
				<div class="article"><p>"The app has a user-friendly interface and it is very easy to use. I've never had a problem so far."</p><p>Serdar Arslan<br/>Owner<br/>Bosphore</p></div>
			</div>
		</div>
	</div>
</div>

<div id="light_grey" class="border_bottom">
	<div id="container" >
		<div class="left width_100 pad-bottom">
			<h2 class="padding_top_40 pad_left">Aurora Experience <span><img src="assets/images/page_assets/case_studies/usa_ico.png" alt=""><img src="assets/images/page_assets/case_studies/canada_ico.png" alt=""></span></h2>
			<p>
				Aurora Experience Inc. specializes in selling designer outdoor lighting systems and is the North American distributor for Techmar lighting from Holland. Aurora Experience uses the Onsight mobile sales app to showcase their products at tradeshows. The hi-res images in the app makes it easier to show visitors the entire product range in their natural setting.
			</p>
			<span class="left_auto_width float_left"><a href="#"><input type="submit" class="pastle_blue_btn" value="View Case Study"></a></span>
		</div>
		<div class="right pad-top">
			<img class="display_none fit" src="assets/images/page_assets/case_studies/aurora.png">
		</div>
	</div>
	<br style="clear:both;"/>
</div>

<div id="full_white">
	<div id="container">
		<div class="left pad-top pad-bottom">
			<img class="display_none fit" src="assets/images/page_assets/case_studies/steins.png">
		</div>
		<div class="right pad-top width_100">
			<h2 class="padding_top_40 pad_left">Steins Foods <span><img src="assets/images/page_assets/case_studies/uk_ico.png" alt=""></span></h2>
			<p>
				Steins Foods is a London based importer and distributor of delicious gourmet food. They source their unique products from all over the world while still supporting local products from suppliers within the United Kingdom. Steins Foods is using the Onsight sales app to automate their order processing when selling to grocery stores, health food stores and delicatessens all across the UK.
			</p>
			<span class="left_auto_width float_left"><a href="#"><input type="submit" class="pastle_blue_btn" value="View Case Study"></a></span>
		</div>
	</div>
</div>

<div id="light_grey" class="border_bottom">
	<div id="container" >
		<div class="left width_100 pad-top">
			<h2 class="padding_top_40 pad_left">Lewis of London <span><img src="assets/images/page_assets/case_studies/uk_ico.png" alt=""></span></h2>
			<p>
				Lewis of London wanted to use technology to make their field-based sales team more efficient whilst still maintaining that close-knit family feeling in the way they do business. They needed a smoother and more efficient ordering process that would easily integrate into the way they were currently doing business.
			</p>
			<span class="left_auto_width float_left"><a href="#"><input type="submit" class="pastle_blue_btn" value="View Case Study"></a></span>
		</div>
		<div class="right pad-top pad-bottom">
			<img class="display_none fit" src="assets/images/page_assets/case_studies/lewis.png">
		</div>
	</div>
	<br style="clear:both;"/>
</div>

<div id="full_white">
	<div id="container">
		<div class="left pad-top pad-bottom">
			<img class="display_none fit" src="assets/images/page_assets/case_studies/escape.png">
		</div>
		<div class="right pad-top width_100">
			<h2 class="padding_top_40 pad_left">Escape to Paradise <span><img src="assets/images/page_assets/case_studies/au_ico.png" alt=""></span></h2>
			<p>
				Escape to Paradise designs and distributes luxury home decor items and clothing accessories to help bring that holiday feeling to people in their daily lives. This rapidly-growing company needed a way to send up-to-date copies of their product catalog to their agents and distributors spread all across Australia and New Zealand.
			</p>
			<span class="left_auto_width float_left"><a href="#"><input type="submit" class="pastle_blue_btn" value="View Case Study"></a></span>
		</div>
	</div>
</div>

<div id="light_grey" class="border_bottom">
	<div id="container" >
		<div class="left width_100 pad-top">
			<h2 class="padding_top_40 pad_left">Bosphore <span><img src="assets/images/page_assets/case_studies/canada_ico.png" alt=""></span></h2>
			<p>
				Bosphore is a sales and distribution company based in Montreal, Québec, Canada. This company imports products from Europe and the United States for distribution all across Canada. Bosphore wanted a system and process in place that could have orders captured and processed by the company as quickly and accurately as possible.
			</p>
			<span class="left_auto_width float_left"><a href="#"><input type="submit" class="pastle_blue_btn" value="View Case Study"></a></span>
		</div>
		<div class="right pad-top pad-bottom">
			<img class="display_none fit" src="assets/images/page_assets/case_studies/bosphore.png">
		</div>
	</div>
	<br style="clear:both;"/>
</div>

<div id="full_white">
	<div id="container">
		<div class="left pad-top pad-bottom">
			<img class="display_none fit" src="assets/images/page_assets/case_studies/supreme.png">
		</div>
		<div class="right pad-top width_100">
			<h2 class="padding_top_40 pad_left">Supreme Beverages <span><img src="assets/images/page_assets/case_studies/za_ico.png" alt=""></span></h2>
			<p>
				Supreme Beverages is a new and innovative liquor brand. Since their sales reps sell to clubs, bars and liquor stores spread over a wide area, they needed a catalog and ordering solution that would provide them the ability to place orders from anywhere. They also wanted a way to showcase their products in an appealing manner to new customers.
			</p>
			<span class="left_auto_width float_left"><a href="#"><input type="submit" class="pastle_blue_btn" value="View Case Study"></a></span>
		</div>
	</div>
</div>


<div id="full_white" class="margin_up">
	<div id="container">
		<div class="services_full need_more_info infoblock">
			<div class="left_40">
				<img class="display_none width_tab" src="assets/images/page_assets/mobile_sales/need_info.png">
			</div>
			<div class="right_60 width_100">
				<h2>Get more information on our mobile commerce app</h2>
				<form action="#">
					<input type="text" name="full_name" value="" placeholder="Full Name">
					<input type="text" name="email" value="" placeholder="Email">
					<textarea placeholder="Please send me more information on your mobile commerce app"></textarea>
					<p>Devices Required</p>
					<div>
						<div class="cblabel"><label><input type="checkbox" name="devices" value="" id="a"/>iPhone</label><br/>
							 <label><input type="checkbox" name="devices" value="" id="d"/>iPad</label>
						</div>
						<div class="cblabel"><label><input type="checkbox" name="devices" value="" id="b"/>Android Phone</label><br/>
							 <label><input type="checkbox" name="devices" value="" id="e"/>Android Tablet</label>
						</div>
						<div class="cblabel">
							 <label><input type="checkbox" name="devices" value="" id="f"/>Windows Tablet</label><br/>
							 <label><input type="checkbox" name="devices" value="" id="c"/>Windows Phone</label>
						</div>
					</div>
					<span class="right_auto_width txt_right_106"><a href="#"><input type="submit" value="Submit" class="green_btn"></a></span>
				</form>
			</div>
		</div>
	</div>
</div>

<footer>
	<div class="mid">
		<div id="footer_menu" class="display_none">
			<ul>
				<li><a href="#">Mobile Sales</a>
					<ul>
						<li><a href="mobile-sales-app.php">Overview</a></li>
						<li><a href="mobile-sales-app-features.php">App Features</a></li>
						<li><a href="mobile-sales-app-benefits.php">App Benefits</a></li>
						<li><a href="mobile-catalog.php">Mobile Catalogue</a></li>
						<li><a href="mobile-sales-apps-for-ipad-android-windows.php">Download Apps</a></li>
						<li><a href="mobile-sales-app-pricing.php">App Pricing</a></li>
					</ul>
				</li>
			</ul>
			<ul>
				<li><a href="#">Mobile Commerce</a>
					<ul>
						<li><a href="mobile-commerce-app.php">Overview</a></li>
						<li><a href="mobile-commerce-app-features.php">Features</a></li>
						<li><a href="mobile-commerce-app-benefits.php">Benefits</a></li>
						<li><a href="mobile-commerce-app-how-it-works.php">How it works</a></li>
						<li><a href="mobile-commerce-app-platforms.php">Platforms</a></li>
						<li><a href="mobile-commerce-app-pricing.php">Pricing</a></li>
					</ul>
				</li>
			</ul>
			<ul>
				<li><a href="#">Mobile Merchandising</a>
					<ul>
						<li><a href="mobile-merchandising.php">Overview</a></li>
						<li><a href="mobile-merchandising-features.php">Features</a></li>
					</ul>
				</li>
			</ul>
			<ul>
				<li><a href="#">About</a>
					<ul>
						<li><a href="who-we-are.php">Who we are</a></li>
						<li><a href="#">Our Blog</a></li>
						<li><a href="services.php">Our Services</a></li>
						<li><a href="partner.php">Partner Program</a></li>
					</ul>
				</li>
			</ul>
			<ul class="last_ul">
				<li><a href="mobile-case-studies.php">Case Studies</a></li>
				<li><a href="sign-up.php">Sign Up</a></li>
				<li><a href="#">My Account</a></li>
				<li><a href="contact-us.php">Contact</a></li>
				<li><a href="index.php">Home</a></li>
			</ul>
		</div>
		<div id="footer_left">
			<div id="footer_logo">
				<h1><a href="#">OnsightApp</a></h1>
			</div>
			<div id="social">
				<a href="#"><img src="assets/images/ux/facebook_ico.png"></a>
				<a href="#"><img src="assets/images/ux/google_ico.png"></a>
				<a href="#"><img src="assets/images/ux/twitter_ico.png"></a>
			</div>
			<div id="copy">
				<p>Copyright 2014</p>
			</div>
		</div>
	</div>
</footer>
<a class="sidetag quote_1" href="sign-up.php">
    SIGN UP NOW
</a>
<a class="sidetag signup_1" href="#getquote_sidetag">
    QUESTIONS ?
</a>

<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="assets/js/slick.min.js"></script>

<script>
$(document).ready(function(){
	$('.single-item').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1
    });
});
</script>
</body>
</html>