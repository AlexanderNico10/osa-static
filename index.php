<!doctype html>
<head>
	<title>Onsight Mobile Sales App, Mobile Commerce, Mobile Merchandising</title>
	<meta content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
	<meta name="description" content="The Onsight suite of mobile business apps delivers critical business functionality on your mobile devices. Our enterprise mobile solutions help make your offsite mobile workers productive.">
	<meta name="keywords" content="mobile business software; enterprise app; sales; mobile merchandising; mobile commerce">
	<link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" media="screen and (max-width: 640px)" href="assets/css/small.css" />
	<link rel='stylesheet' media='screen and (min-width: 320px) and (max-width: 640px)' href='assets/css/small.css' />
	<link rel='stylesheet' media='screen and (min-width: 640px) and (max-width: 1024px)' href='assets/css/medium.css' />
	<link rel="stylesheet" type="text/css" href="assets/css/bjqs.css">
	<script src="assets/js/jquery.min.js" ></script>
 	<script src="assets/js/bootstrap.js"></script>
 	<script src="assets/js/bjqs-1.3.js"></script>
	<script src="assets/js/bootstrap.js"></script>
</head>
<body>
<header>
	<div id="container">
		<div id="logo"><h1><a href="/">Onsightapp</a></h1></div>
		<div id="menu">
			<ul>
				<li><a href="index.php" class="current">Home</a></li>
				<li><a href="mobile-sales-app.php">Mobile Sales</a>
					<ul>
						<li><a href="mobile-sales-app-features.php">App Features</a></li>
						<li><a href="mobile-sales-app-benefits.php">App Benefits</a></li>
						<li><a href="mobile-catalog.php">Mobile Catalogue</a></li>
						<li><a href="mobile-sales-apps-for-ipad-android-windows.php">Download Apps</a></li>
						<li><a href="mobile-sales-app-pricing.php">App Pricing</a></li>
					</ul>
				</li>
				<li><a href="mobile-merchandising.php">Mobile Merchandising</a>
					<ul>
						<li><a href="mobile-merchandising-features.php">App Features</a></li>
					</ul>
				</li>
				<li><a href="mobile-commerce-app.php">Mobile Commerce</a>
					<ul>
						<li><a href="mobile-commerce-app-features.php">Features</a></li>
						<li><a href="mobile-commerce-app-benefits.php">Benefits</a></li>
						<li><a href="mobile-commerce-app-how-it-works.php">How it works</a></li>
						<li><a href="mobile-commerce-app-platforms.php">Platforms</a></li>		
						<li><a href="mobile-commerce-app-pricing.php">Pricing</a></li>
					</ul>
				</li>
				<li><a href="mobile-case-studies.php">Case Studies</a></li>
				<li><a href="#">About</a>
					<ul>
						<li><a href="who-we-are.php">Who We Are</a></li>
						<li><a href="services.php">Our Services</a></li>
						<li><a href="partner.php">Partner Programme</a></li>
					</ul>
				</li>
				<li><a href="contact-us.php">Contact Us</a></li>
			</ul>
		</div>
		<nav class="navbar navbar-default" role="navigation">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">Onsightapp</a>
		    </div>
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li class="active"><a href="#">Home</a></li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mobile Sales <b class="caret"></b></a>
				  <ul class="dropdown-menu">
		            <li><a href="mobile-sales-app-features.php">App Features</a></li>
					<li><a href="mobile-sales-app-benefits.php">App Benefits</a></li>
					<li><a href="mobile-catalog.php">Mobile Catalogue</a></li>
					<li><a href="mobile-sales-apps-for-ipad-android-windows.php">Download Apps</a></li>
					<li><a href="mobile-sales-app-pricing.php">App Pricing</a></li>
		          </ul>
		        </li>
		          <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mobile Merchandising <b class="caret"></b></a>
		          <ul class="dropdown-menu">
		           	<li><a href="mobile-merchandising-features.php">App Features</a></li>
		          </ul>
		        </li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mobile Commerce <b class="caret"></b></a>
				  <ul class="dropdown-menu">
		           	<li><a href="mobile-commerce-app-features.php">Features</a></li>
					<li><a href="mobile-commerce-app-benefits.php">Benefits</a></li>
					<li><a href="mobile-commerce-app-how-it-works.php">How it works</a></li>
					<li><a href="mobile-commerce-app-platforms.php">Platforms</a></li>		
					<li><a href="mobile-commerce-app-pricing.php">Pricing</a></li>
		          </ul>
		        </li>
		        <li><a href="mobile-case-studies.php">Case Studies</a></li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">About<b class="caret"></b></a>
		          <ul class="dropdown-menu">
					<li><a href="who-we-are.php">Who We Are</a></li>
					<li><a href="services.php">Our Services</a></li>
					<li><a href="partner.php">Partner Programme</a></li>
		          </ul>
		        </li>
		        <li><a href="contact-us.php">Contact Us</a></li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	</div>
</header>
<div id="slider_container" class="slider-wrapper">
		<ul class="bjqs">
			<li>
				<div class="slides">
					<div class="slides_content">
						<h2>Sales management software</h2>
						<p>Manage your client information and all sales information in one central database</p>
						<a href="#" class="slides_link">Read more</a>
					</div>
					<div class="slides_img"><img src="assets/images/slider.jpg" alt="image01"></div>
				</div>
			</li>
			<li>
				<div class="slides">
					<div class="slides_content">
						<h2>Easy management</h2>
						<p>Access to a menu-driven subset of switch configuration and performance features</p>
						<a href="#" class="slides_link">Read more</a>
					</div>
					<div class="slides_img"><img src="assets/images/slider1.jpg" alt="image01"></div>
				</div>
			</li>
		</ul>
</div>
<div id="wrapper">
	<div id="content">
		<h1 class="mobile">Mobile Business App</h1>
		<div id="block">
			<img class="width_tab" src="assets/images/tablet1.png">
			<h2>B2B MOBILE SALES APP</h2>
			<p class="p_height">Close deals faster using the Onsight mobile sales app. Use the app to manage client info, demo products, create quotes and place orders. Available on Android, iOS and Windows.</p>
			<a href="mobile-sales-app-features.php"><button class="orange_btn orange_btn_index">Learn More</button></a>
			<a href="sign-up.php"><button class="blue_btn blue_btn_index">Sign Up</button></a>
		</div>
		<div id="block">
			<img class="width_tab" src="assets/images/tablet2.png">
			<h2>B2C MOBILE COMMERCE APP</h2>
			<p class="p_height">Publish your own branded mobile app in the app stores for Android, iOS and Windows. Showcase your products, list your retail outlets and even sell your products via the app.</p>
			<a href="mobile-commerce-app-features.php"><button class="orange_btn orange_btn_index learn-button-margin">Learn More</button></a>
			<a href="contact-us.php"><button class="blue_btn blue_btn_index">Contact Us for a Quote</button></a>
		</div>
		<div id="block">
			<img class="width_tab" src="assets/images/tablet3.png">
			<h2>MOBILE MERCHANDISING APP</h2>
			<p class="p_height">Let you merchandisers and outside sales reps perform retail audits and merchandising inspections using their mobile device.</p>
			<a href="mobile-merchandising-features.php"><button class="orange_btn orange_btn_index learn-margin">Learn More</button></a>
			<a href="contact-us.php"><button class="blue_btn blue_btn_index">Contact Us for a Quote</button></a>
		</div>
	</div>
</div>
<div id="cta">
	<div class="mid">
		<div id="download">
			<h3>Download</h3>
			<ul>
				<li><a href="#"><img class="logo_download_img" src="assets/images/google_logo.png"></a></li>
				<li><a href="#"><img class="logo_download_img" src="assets/images/app_store_logo.png"></a></li>
				<li><a href="#"><img class="logo_download_img" src="assets/images/windows_store.png"></a></li>
			</ul>
		</div>
	</div>
</div>
<div id="blog" class="mid pad_left">
	<h3>Blog</h3>
	<ul>
		<li>
			<img src="assets/images/example_blog1.jpg">
			<h4>INVOICING CLIENTS CORRECTLY</h4>
			<p>Creating invoices with the correct details on it can save you and your customers a lot of time. If you have the correct details in place on the invoice, no one has to phone around to find out any information...</p>
			<a href="#">Read More></a>
		</li>
		<li>
			<img src="assets/images/example_blog2.jpg">
			<h4>CREATING A GREAT SALES PITCH</h4>
			<p>The word "pitch" is something that shouldn't, in fact, be used anymore because of the negative connotations that are associated with it. It is an old-timey word that was used to describe salespeople...</p>
			<a href="#">Read More></a>
		</li>
		<li>
			<img src="assets/images/example_blog3.jpg">
			<h4>WHAT TO LOOK FOR IN A SUPPLIER</h4>
			<p>Finding a good, trustworthy supplier to do business with is invaluable. You will inevitable have to develop a long-term relationship with them so choose your partner here well...</p>
			<a href="#">Read More></a>
		</li>
	</ul>
</div>
<footer>
	<div class="mid">
		<div id="footer_menu" class="display_none1">
			<ul>
				<li><a href="#">Mobile Sales</a>
					<ul>
						<li><a href="mobile-sales-app.php">Overview</a></li>
						<li><a href="mobile-sales-app-features.php">App Features</a></li>
						<li><a href="mobile-sales-app-benefits.php">App Benefits</a></li>
						<li><a href="mobile-catalog.php">Mobile Catalogue</a></li>
						<li><a href="mobile-sales-apps-for-ipad-android-windows.php">Download Apps</a></li>
						<li><a href="mobile-sales-app-pricing.php">App Pricing</a></li>
					</ul>
				</li>
			</ul>
			<ul>
				<li><a href="#">Mobile Commerce</a>
					<ul>
						<li><a href="mobile-commerce-app.php">Overview</a></li>
						<li><a href="mobile-commerce-app-features.php">Features</a></li>
						<li><a href="mobile-commerce-app-benefits.php">Benefits</a></li>
						<li><a href="mobile-commerce-app-how-it-works.php">How it works</a></li>
						<li><a href="mobile-commerce-app-platforms.php">Platforms</a></li>
						<li><a href="mobile-commerce-app-pricing.php">Pricing</a></li>
					</ul>
				</li>
			</ul>
			<ul>
				<li><a href="#">Mobile Merchandising</a>
					<ul>
						<li><a href="mobile-merchandising.php">Overview</a></li>
						<li><a href="mobile-merchandising-features.php">Features</a></li>
					</ul>
				</li>
			</ul>
			<ul>
				<li><a href="#">About</a>
					<ul>
						<li><a href="who-we-are.php">Who we are</a></li>
						<li><a href="#">Our Blog</a></li>
						<li><a href="services.php">Our Services</a></li>
						<li><a href="partner.php">Partner Program</a></li>
					</ul>
				</li>
			</ul>
			<ul class="last_ul">
				<li><a href="mobile-case-studies.php">Case Studies</a></li>
				<li><a href="sign-up.php">Sign Up</a></li>
				<li><a href="#">My Account</a></li>
				<li><a href="contact-us.php">Contact</a></li>
				<li><a href="index.php">Home</a></li>
			</ul>
		</div>
		<div id="footer_left">
			<div id="footer_logo">
				<h1><a href="#">OnsightApp</a></h1>
			</div>
			<div id="social">
				<a href="#"><img class="logo_connect" src="assets/images/ux/facebook_ico.png" width="27"></a>
				<a href="#"><img class="logo_connect" src="assets/images/ux/google_ico.png" width="27"></a>
				<a href="#"><img class="logo_connect" src="assets/images/ux/twitter_ico.png" width="27"></a>
			</div>
			<div id="copy">
				<p>Copyright 2014</p>
			</div>
		</div>
	</div>
</footer>
<a class="sidetag quote_1" href="sign-up.php">
    SIGN UP NOW
</a>
<a class="sidetag signup_1" href="#getquote_sidetag">
    QUESTIONS ?
</a>

    <script type="text/javascript">
    jQuery(document).ready(function($) {
	var width = $( document ).width();
    $('#slider_container').bjqs({
    	'animtype': 'slide',
        'width' : width,
        'responsive' : true,
        'showcontrols' : false,
        'nexttext' : 'Next', // Text for 'next' button (can use HTML)
		'prevtext' : 'Prev', // Text for 'previous' button (can use HTML)
        'hoverpause' : true,
        'showmarkers' : false
    	});
	});
    </script>
</body>
</html>