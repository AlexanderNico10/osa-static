<!doctype html>
<head>
	<title>Mobile Sales App Software - Onsight</title>
	<meta content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
	<meta name="description" content="Use your tablet to demo your product catalogue and brochures to clients, and to create and submit quotes and orders whilst on the road.">
	<meta name="keywords" content="mobile sales app; product catalogue; crm; orders; quotes; contracts; proposals">
	<link href="http://getbootstrap.com/dist/css/bootstrap.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="assets/css/slick.css"/>
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<link rel="stylesheet" media="screen and (max-width: 640px)" href="assets/css/small.css" />
	<link rel='stylesheet' media='screen and (min-width: 320px) and (max-width: 640px)' href='assets/css/small.css' />
	<link rel='stylesheet' media='screen and (min-width: 640px) and (max-width: 1024px)' href='assets/css/medium.css' />
	<link rel="stylesheet" href="assets/css/default/default.css" type="text/css" media="screen" />
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" ></script>
 	<script src="http://getbootstrap.com/dist/js/bootstrap.js"></script>
	<link rel="stylesheet" href="assets/css/colorbox.css" />
	<script src="assets/js/jquery.colorbox.js"></script>

	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
 	<script src="assets/js/jquery.min.js" ></script>	
 	<script src="assets/js/bootstrap.js" ></script>

	<script>
		$(document).ready(function(){
			$(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390});
		});
	</script>
</head>
<body>
<header>
	<div id="container">
		<div id="logo"><h1><a href="/">Onsightapp</a></h1></div>
		<div id="menu">
			<ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="mobile-sales-app.php" class="current">Mobile Sales</a>
					<ul>
						<li><a href="mobile-sales-app-features.php">App Features</a></li>
						<li><a href="mobile-sales-app-benefits.php">App Benefits</a></li>
						<li><a href="mobile-catalog.php">Mobile Catalogue</a></li>
						<li><a href="mobile-sales-apps-for-ipad-android-windows.php">Download Apps</a></li>
						<li><a href="mobile-sales-app-pricing.php">App Pricing</a></li>
					</ul>
				</li>
				<li><a href="mobile-merchandising.php">Mobile Merchandising</a>
					<ul>
						<li><a href="mobile-merchandising-features.php">App Features</a></li>
					</ul>
				</li>
				<li><a href="mobile-commerce-app.php">Mobile Commerce</a>
					<ul>
						<li><a href="mobile-commerce-app-features.php">Features</a></li>
						<li><a href="mobile-commerce-app-benefits.php">Benefits</a></li>
						<li><a href="mobile-commerce-app-how-it-works.php">How it works</a></li>
						<li><a href="mobile-commerce-app-platforms.php">Platforms</a></li>		
						<li><a href="mobile-commerce-app-pricing.php">Pricing</a></li>
					</ul>
				</li>
				<li><a href="mobile-case-studies.php">Case Studies</a></li>
				<li><a href="#">About</a>
					<ul>
						<li><a href="who-we-are.php">Who We Are</a></li>
						<li><a href="services.php">Our Services</a></li>
						<li><a href="partner.php">Partner Programme</a></li>
					</ul>
				</li>
				<li><a href="contact-us.php">Contact Us</a></li>
			</ul>
		</div>
		<nav class="navbar navbar-default" role="navigation">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">Onsightapp</a>
		    </div>
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li class="active"><a href="index.php">Home</a></li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mobile Sales <b class="caret"></b></a>
		          <ul class="dropdown-menu">
		            <li><a href="mobile-sales-app-features.php.php">App Features</a></li>
						<li><a href="mobile-sales-app-benefits.php">App Benefits</a></li>
						<li><a href="mobile-catalog.php">Mobile Catalogue</a></li>
						<li><a href="mobile-sales-apps-for-ipad-android-windows.php">Download Apps</a></li>
						<li><a href="mobile-sales-app-pricing.php">App Pricing</a></li>
		          </ul>
		        </li>
		          <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mobile Merchandising <b class="caret"></b></a>
		          <ul class="dropdown-menu">
		           	<li><a href="mobile-merchandising-features.php">App Features</a></li>
		          </ul>
		        </li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mobile Commerce <b class="caret"></b></a>
		          <ul class="dropdown-menu">
		           	<li><a href="mobile-commerce-app-features.php">Features</a></li>
					<li><a href="mobile-commerce-app-benefits.php">Benefits</a></li>
					<li><a href="mobile-commerce-app-how-it-works.php">How it works</a></li>
					<li><a href="mobile-commerce-app-platforms.php">Platforms</a></li>		
					<li><a href="mobile-commerce-app-pricing.php">Pricing</a></li>
		          </ul>
		        </li>
		        <li><a href="mobile-case-studies.php">Case Studies</a></li>
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown">About<b class="caret"></b></a>
		          <ul class="dropdown-menu">
		        	<li><a href="who-we-are.php">Who We Are</a></li>
					<li><a href="services.php">Our Services</a></li>
					<li><a href="partner.php">Partner Programme</a></li>
		          </ul>
		        </li>
		        <li><a href="contact-us.php">Contact Us</a></li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	</div>
</header>
<div id="breadcrumbs">
	<ul>
		<li>Mobile Sales Application</li>
	</ul>
</div>
<div id="wrapper">
	<div id="container">
		<div class="left noFloat pad_left">
			<h2 class="width_58 width_100">For wholesalers, manufacturers & distributors</h2>
			<p class="width_75 width_100 font_14">Use your tablet to demo your product catalogue and brochures to clients and to create and submit quotes and orders whilst on the road. Works on iPad's, iPad Mini's, Windows 8 tablets, 7- and 10-inch Android tablets and Android phones.</p>
		</div>
		<div class="right noFloat">
			<div id="ytvideo1" style="display:none;">
    			<iframe width="459" height="350" id="ytvideo" frameborder="0" allowfullscreen></iframe>
			</div>
			<a class='youtube' href="http://www.youtube.com/embed/MCe-QoE3Qa4?rel=0&wmode=transparent"><img class="fit what_we_do" src="assets/images/page_assets/mobile_sales/play_video.jpg"></a>
		</div>
	</div>
</div>
<div id="middle_grey" class="padding_bottom_10">
	<div id="container">
		<div class="full application pad_left">
			<h2 class="center pad-top no_padding">Try it Now for FREE</h2>
			<form action="#" id="free_trial">
				<input type="text" value="" placeholder="Full Name">
				<input type="text" value="" placeholder="Company Names">
				<input type="text" value="" placeholder="Email Address">
				<input type="text" value="" placeholder="Password">
				<a href="#"><input type="submit" value="Start Free Trial" class="green_btn"></a>
			</form>
			<p class="center">*We promise that your contact details will never be shared with anyone else! Read our <a href="#">Privacy Policy</a></p>
			<p class="center">*By signing up you agree to accept our <a href="#">Terms and Conditions</a></p>
			<ul>
				<li>
					<img class="logo_img" src="assets/images/page_assets/mobile_sales/money_back.png">
				</li>
				<li>
					<img class="logo_img" src="assets/images/page_assets/mobile_sales/free.png">
				</li>
				<li>
					<img class="logo_img" src="assets/images/page_assets/mobile_sales/no_card.png">
				</li>
			</ul>
		</div>
	</div>
</div>
<div id="middle_blue" class="padding_bottom_10">
	<div id="container">
		<div class="full application pad_left">
			<h2 class="center pad-top white_txt">Getting started with Onsiteapp is as easy as:</h2>
			<ul class="blue_three_steps">
				<li>
					<img class="steps" src="assets/images/page_assets/mobile_sales/3_steps.png">
				</li>
			</ul>
		</div>
	</div>
</div>

<div id="wrapper">
	<div id="sub_content" class="testimonials display_none">
		<div id="testimonials_scroller" class="quotes"> 
			<h2 class="padding_bottom_10 padding_left_135 no_padding pad80 padding_left_var">What our clients are saying</h2>
			<div class="slider single-item">
				<div class="article"><p>"I found the app a very useful tool in showcasing my product range."</p><p>Siggi Helgason,<br/>Owner<br/>Aurora Experience Inc</p></div>
				<div class="article"><p>"I would recommend any field-based sales team to try the Onsight app."</p><p>Ryan Cox,<br/>Sales Manager<br/>Lewis of London</p></div>
				<div class="article"><p>"Onsight allows me to update my product line at any time."</p><p>Sacha Alagich,<br/>Founder and Owner<br/>Escape to Paradise</p></div>
				<div class="article"><p>"The Onsight mobile sales system is helping us cope with our rapid growth."</p><p>Steve Steinhardt,<br/>Managing Director<br/>Steins Foods Ltd</p></div>
				<div class="article"><p>"The app has a user-friendly interface and it is very easy to use. I've never had a problem so far."</p><p>Serdar Arslan<br/>Owner<br/>Bosphore</p></div>
			</div>
		</div>
	</div>
</div>

<div id="full_white">
	<div id="container" class="improve_sales padding_top_20 pad_left">
		<div class="services_full padding_bottom_10 center_mob">
			<h2 class="center no_padding">Onsight Helps Improve Sales</h2>
			<h4 class="blue center">Boost your sales numbers, save costs and track sales actions.</h4>
			<ul>
				<li>
					<img src="assets/images/page_assets/mobile_sales/handshake.png">
					<h5>Close deals faster.</h5><p>Quickly create and send quotes and orders whilst on the road.</p>
				</li>
				<li>
					<img src="assets/images/page_assets/mobile_sales/cloud_download.png">
					<h5>Access your accounts</h5><p>and contacts information from anywhere using any device.</p>
				</li>
				<li>
					<img src="assets/images/page_assets/mobile_sales/graph.png">
					<h5>Improve sales performance.</h5><p>Track and manage all sales activities effortlessly.</p>
				</li>
				<li>
					<img src="assets/images/page_assets/mobile_sales/time.png">
					<h5>Save time and money.</h5><p>Eliminate printed quotes, orders and catalogues.</p>
				</li>
			</ul>
			<a href="#"><button class="full_blue_btn center_full">Learn More</button></a>
		</div>
	</div>
</div>
<div id="dark_grey_full">
	<div id="container">
		<div id="text">
			<h2 class="white_txt pad_left texth2p">FREE FOR 1 USER</h2>
			<p class="texth2p">
				LOW MONTHLY FEE
				FOR EXTRA USERS
			</p>
			<a href="#"><button class="green_btn float_right margin_right_110">Compare editions and prices</button></a>
		</div>
	</div>
</div>
<div id="full_white">
	<div id="container">
		<div class="services_full product_cats">
			<h2 class="center no_padding">Product Catalogue, Quotes & Orders</h2>
			<h4 class="center">Give your sales team the right tools to close more deals.</h4>
			<ul>
				<li>
					<img class="logo_img_top" src="assets/images/page_assets/mobile_sales/catalog.png">
					<h5>Mobile Product Catalogue</h5><p>Showcase your products using a glossy, visually-appealing electronic catalogue.</p>
				</li>
				<li>
					<img class="logo_img_top" src="assets/images/page_assets/mobile_sales/brochure.png">
					<h5>Electronic Brochures</h5><p>Convert your existing printed brochures into electronic brochures</p>
				</li>
				<li>
					<img class="logo_img_top" src="assets/images/page_assets/mobile_sales/accounts.png">
					<h5>Accounts & Contacts</h5><p>Manage all your client information in one shared database.</p>
				</li>
				<li>
					<img class="logo_img_top" src="assets/images/page_assets/mobile_sales/quote.png">
					<h5>Quotes & Orders</h5><p>Create a new quote or order whilst onsite at your client's premises.</p>
				</li>
			</ul>
			<a href="#"><button class="full_blue_btn center_full">See All Features</button></a>
		</div>
	</div>
</div>

<div id="full_white" class="get_onsight_app border_bottom">
	<div id="container">
		<div class="services_full no_pad">
			<h2 class="no_padding padding_top_40 padding_left_30">Get the Onsight Apps</h2>
			<ul>
				<li>
					<img class="width_tab" src="assets/images/tablet1.png">
					<h5><a href="https://play.google.com/store/apps/details?id=maxxor.onsight">DOWNLOAD ONSIGHT FOR ANDROID</a></h5>
					<p>
						<img src="assets/images/google_logo.png">
					</p>
					<p><a href="http://apps.samsung.com/mars/topApps/topAppsDetail.as?productId=000000660982">Available in Samsung store</a></p>
				</li>
				<li>
					<img class="width_tab" src="assets/images/tablet2.png">
					<h5><a href="https://itunes.apple.com/za/app/onsight-mobile-sales-app-for/id607594814?mt=8">DOWNLOAD ONSIGHT FOR IPAD</a></h5>
					<p>
						<img src="assets/images/app_store_logo.png">
					</p>
				</li>
				<li>
					<img class="width_tab" src="assets/images/tablet3.png">
					<h5><a href="http://apps.microsoft.com/windows/en-us/app/onsight-mobile-sales/bae9c4d3-7696-4b7d-8652-f3d0b31a9c47">DOWNLOAD ONSIGHT FOR WINDOWS</a></h5>
					<p>
						<img src="assets/images/windows_store.png">
					</p>
				</li>
			</ul>
			
		</div>
	</div>
</div>
<div id="full_white" class="margin_up">
	<div id="container">
		<div class="services_full need_more_info width_tab">
			<div class="left_40 display_none">
				<img class="display_none width_tab" src="assets/images/page_assets/mobile_sales/need_info.png">
			</div>
			<div class="right_60 width_100 pad_left">
				<h2>Complex requirements? Contact us to discuss</h2>
				<form action="#">
					<input type="text" name="full_name" value="" placeholder="Full Name">
					<input type="text" name="email" value="" placeholder="Email">
					<textarea placeholder="Please send me more information"></textarea>
					<span class="right_auto_width txt_right_106"><a href="#"><input type="submit" value="Submit" class="green_btn"></a></span>
				</form>
			</div>
		</div>
	</div>
</div>
<footer>
	<div class="mid">
		<div id="footer_menu" class="display_none">
			<ul>
				<li><a href="#">Mobile Sales</a>
					<ul>
						<li><a href="mobile-sales-app.php">Overview</a></li>
						<li><a href="mobile-sales-app-features.php">App Features</a></li>
						<li><a href="mobile-sales-app-benefits.php">App Benefits</a></li>
						<li><a href="mobile-catalog.php">Mobile Catalogue</a></li>
						<li><a href="mobile-sales-apps-for-ipad-android-windows.php">Download Apps</a></li>
						<li><a href="mobile-sales-app-pricing.php">App Pricing</a></li>
					</ul>
				</li>
			</ul>
			<ul>
				<li><a href="#">Mobile Commerce</a>
					<ul>
						<li><a href="mobile-commerce-app.php">Overview</a></li>
						<li><a href="mobile-commerce-app-features.php">Features</a></li>
						<li><a href="mobile-commerce-app-benefits.php">Benefits</a></li>
						<li><a href="mobile-commerce-app-how-it-works.php">How it works</a></li>
						<li><a href="mobile-commerce-app-platforms.php">Platforms</a></li>
						<li><a href="mobile-commerce-app-pricing.php">Pricing</a></li>
					</ul>
				</li>
			</ul>
			<ul>
				<li><a href="#">Mobile Merchandising</a>
					<ul>
						<li><a href="mobile-merchandising.php">Overview</a></li>
						<li><a href="mobile-merchandising-features.php">Features</a></li>
					</ul>
				</li>
			</ul>
			<ul>
				<li><a href="#">About</a>
					<ul>
						<li><a href="who-we-are.php">Who we are</a></li>
						<li><a href="#">Our Blog</a></li>
						<li><a href="services.php">Our Services</a></li>
						<li><a href="partner.php">Partner Program</a></li>
					</ul>
				</li>
			</ul>
			<ul class="last_ul">
				<li><a href="mobile-case-studies.php">Case Studies</a></li>
				<li><a href="sign-up.php">Sign Up</a></li>
				<li><a href="#">My Account</a></li>
				<li><a href="contact-us.php">Contact</a></li>
				<li><a href="index.php">Home</a></li>
			</ul>
		</div>
		<div id="footer_left">
			<div id="footer_logo">
				<h1><a href="#">OnsightApp</a></h1>
			</div>
			<div id="social">
				<a href="#"><img src="assets/images/ux/facebook_ico.png"></a>
				<a href="#"><img src="assets/images/ux/google_ico.png"></a>
				<a href="#"><img src="assets/images/ux/twitter_ico.png"></a>
			</div>
			<div id="copy">
				<p>Copyright 2014</p>
			</div>
		</div>
	</div>
</footer>
<a class="sidetag quote_1" href="sign-up.php">
    SIGN UP NOW
</a>
<a class="sidetag signup_1" href="#getquote_sidetag">
    QUESTIONS ?
</a>

<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="assets/js/slick.min.js"></script>

<script>
$(document).ready(function(){
	$('.single-item').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1
    });
});
</script>
</body>
</html>